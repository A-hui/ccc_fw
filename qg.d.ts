declare namespace qg {
    /**用户登录 */
    export function login(object: object): void;
    /**创建原生广告 */
    export function createNativeAd(object: Object): any;
    /**创建视频广告 */
    export function createRewardedVideoAd(object: Object): any;
    /**创建banner广告 */
    export function createBannerAd(object: Object): any
    /**创建互推盒子广告 */
    export function createGamePortalAd(object: Object): any
    /**获取手机系统信息 */
    export function getSystemInfoSync()
    export function createGameBannerAd(object: Object): any
}