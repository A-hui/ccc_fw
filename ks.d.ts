declare namespace ks {
    export function showShareMenu(object: object): void
    /**
     * 登录接口。通过该接口获取的 gameUserId 是快手小游戏的唯一用户id，gameToken 是使用快手小游戏服务器API的唯一验证 token。
     * 
     */
    export function login(object: object): void;

    /**
     * 提前向用户发起授权请求。调用后会立刻弹窗询问用户是否同意授权小程序使用某项功能或获取用户的某些数据，但不会实际调用对应接口。如果用户之前已经同意授权，则不会出现弹窗，直接返回成功。（需要首先调用 ks.login 接口）
     */
    export function authorize(object: object): void;

    /**
     * 获取用户信息。（如果在未授权的情况下请求用户信息，会返回默认数据（空昵称、默认头像））
     */
    export function getUserInfo(object: object): void;

    /**
     * 创建一个图片对象。
     */
    export function createImage(): any;
    /**
     * http协议发送信息
     * @param object http信息
     */
    export function request(object: Object): void;
    /**
     * 监听游戏回到前台
     * @param func 回调方法
     */
    export function onShow(func: Function): void;
    /**
     * 监听游戏退到后台
     * @param func 回调方法
     */
    export function onHide(func: Function): void;
    /**
     * 主动拉起转发，进入选择通讯录界面
     * @param object 参数
     */
    export function shareAppMessage(object?: object)
    /**
     * 分包加载
     * @param name 分包名字
     * @param success 成功回调
     * @param fail 失败回调
     * @param compolete 完成回调
     */
    export function loadSubpackage(object: Object);
    /**阿拉丁埋点 关卡分析接口 */
    export function aldStage(): aldStage;
    export class aldStage {
        static onStart(object: object) { }
        static onRunning(object: object) { }
        static onEnd(object: object) { }
    }
    /**
     * 手机短振动
     * @param type 振动类型
     */
    export function vibrateShort(type: string)
    /**
     * 手机长振动
     */
    export function vibrateLong()
    /**显示消息提示框 */
    export function showToast(Object: object)
    /**创建激励视频广告组件。调用该方法创建的激励视频广告是一个单例。 */
    export function createRewardedVideoAd(Object: obejct)
    /**获取全局唯一的游戏画面录制对象 */
    export function getGameRecorder()
    /**
     * 录屏
     */
    interface GameRecorderManager {
        /** 开始录屏 */
        start(): void;
        /** 暂停录屏 */
        pause(): void;
        /** 继续录屏 */
        resume(): void;
        /** 停止录屏 */
        stop(): void;
        /**发布录屏到快手 */
        publishVideo(Object: object): void;
        /**注册监听录制事件的回调函数。当对应事件触发时，回调函数会被执行 */
        on(stringEvent: string, callback: Function);
        /**取消监听录制事件。当对应事件触发时，该回调函数不再执行。 */
        off(stringEvent: string, callback: Function)
    }
}

/**
 * 用户信息
 */
declare type FriendInfo = {
    avatarUrl: string;
    nickname: string;
    openid: string;
}