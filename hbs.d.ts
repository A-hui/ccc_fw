declare namespace hbs {
    /**用户登录 */
    export function gameLogin(object: object): void;
    /**创建原生广告 */
    export function createNativeAd(object: Object): any;
    /**创建视频广告 */
    export function createRewardedVideoAd(object: Object): any;
}