import GNumber from "../../FW/tools/GNumber";
import BattleManager from "./BattleManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Target extends cc.Component {

    @property(cc.Sprite)
    hp_progress: cc.Sprite = null;
    @property(cc.Sprite)
    hp_background: cc.Sprite = null;

    // @property(Power)
    // power: Power = null;
    @property(cc.Node)
    body: cc.Node = null;
    // @property(cc.Node)
    // progress_bg: cc.Node = null;

    private atk: number = 0;
    private def: number = 0;
    private hp: number = 0;
    private maxHp: number = 0;
    private index: number = 0;
    private speed: number = 1;
    private isDeath: boolean = false;
    private isEnemy: boolean = false;

    initData(index: number) {
        this.index = index

        this.maxHp = 1000
        this.hp = this.maxHp;
        this.setHpBar(this.hp, this.maxHp);

        this.atk = 50
        this.def = 20

        let speed = GNumber.getRandomFloatNum(0.2, 1)
        // this.power.initPower(this, speed, 0);
        this.isDeath = false;
    }

    // 开火
    shoot() {
        BattleManager.getInstance().getShootTarget(this.node.convertToWorldSpaceAR(cc.Vec2.ZERO), this.atk, this.isEnemy, this.index);
    }

    // 受击
    onHurt(atk: number) {
        this.hp -= atk - this.def;
        this.setHpBar(this.hp, this.maxHp);

        let deltaX = -40;
        // if (this.boat.isEnemy) {
        //     deltaX = 40;
        // }
        // this.power.setPause(true);
        let isDeath = this.hp <= 0;
        cc.tween(this.body)
            .by(.2 / BattleManager.getInstance().getFightSpeed(), { x: deltaX }, { easing: 'cubicIn' })
            .by(.2 / BattleManager.getInstance().getFightSpeed(), { x: -deltaX }, { easing: 'cubicOut' })
            .call(() => {
                if (isDeath) {
                    this.onDeath();
                }
            })
            .start();
    }

    // 阵亡
    onDeath() {
        this.isDeath = true;
        this.hp_progress.node.active = false;
        this.hp_background.node.active = false;

        this.body.angle = this.isEnemy ? -20 : 20;
        cc.tween(this.body)
            .to(.6 / BattleManager.getInstance().getFightSpeed(), { y: -200 }, { easing: 'cubicOut' })
            .call(() => {
                this.node.destroy();
            })
            .start()
    }

    /**
     * 更新血条位置
     * @param dt 时间
     */
    public update(dt: number): void {
        if (this.hp_background) {
            if (this.hp_background.fillRange <= this.hp_progress.fillRange) {
                this.hp_background.fillRange = this.hp_progress.fillRange;
            } else {
                this.hp_background.fillRange -= this.speed * dt;
            }
        }
    }

    /**
     * 设置血量进度
     * @param hp 当血量
     * @param maxHp 最大血量
     */
    public setHpBar(hp: number, maxHp: number): void {
        if (this.hp_progress) {
            this.hp_progress.fillRange = hp / maxHp;
        }
    }

    public get GetIsDeath(): boolean {
        return this.isDeath;
    }

    public get GetIsEnemy(): boolean {
        return this.isEnemy;
    }

    public set SetIsEnemy(value: boolean) {
        this.isEnemy = value;
        this.body.color = value ? cc.Color.BLACK : cc.Color.WHITE;
        this.body.scaleX = value ? -1 : 1;
        // this.draw();
    }
}