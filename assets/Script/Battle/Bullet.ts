// --------------------------------------------------------------------
// @author: xxx@xxxxxxx.com(创建模块的人员)
// @description:  （描述）
//     子弹组件
// @create: 2022-01-14 12:00:00

import BattleManager from "./BattleManager";

// --------------------------------------------------------------------
const { ccclass, property } = cc._decorator;

/**子弹飞行的时间 */
const FLY_TIME = 2;
/**重力加速度 距离过近，增加重力形成抛物线 */
const G = 200;

@ccclass
export default class Bullet extends cc.Component {

    /**攻击力 */
    private atk: number = 0
    /**目标 */
    private target: any = null;

    /**X方向速度 */
    private x_speed: number = 0
    /**Y方向速度 */
    private y_speed: number = 0

    /**暂停 */
    private pause: boolean = true
    private t: number = 0

    /**
     * 初始化子弹
     * @param atk 攻击力
     * @param initPos 初始位置
     * @param targetPos 目标位置
     * @param target 目标
     */
    initBullet(atk: number, initPos: cc.Vec2, targetPos: cc.Vec2, target: any) {
        this.target = target;
        this.atk = atk;

        this.x_speed = (targetPos.x - initPos.x) / FLY_TIME;
        this.y_speed = ((targetPos.y - initPos.y) + G * FLY_TIME * FLY_TIME / 2) / FLY_TIME;

        this.node.setPosition(this.node.parent.convertToNodeSpaceAR(initPos));
        let angle = cc.v2(this.x_speed, this.y_speed).signAngle(cc.v2(0, 1));
        this.node.angle = -angle * 180 / Math.PI;
        this.node.scale = 0.5;

        this.pause = false;
    }

    protected update(dt: number): void {
        if (this.pause) return;
        if (this.t < FLY_TIME) {
            let delta = BattleManager.getInstance().getFightSpeed() * dt;
            this.t += delta;
            if (this.t >= FLY_TIME) {
                delta -= this.t - FLY_TIME;
                this.t = FLY_TIME;
            }
            this.node.setPosition(cc.v2(this.node.x + this.x_speed * delta, this.node.y + this.y_speed * delta));
            this.y_speed -= G * delta;
            let angle = cc.v2(this.x_speed, this.y_speed).signAngle(cc.v2(0, 1));
            this.node.angle = -angle * 180 / Math.PI;
            this.node.scale = -this.t * this.t / 2 + this.t + 0.5;
        } else {
            this.pause = true;
            // 击中
            this.target && this.target.isValid && !this.target.isDeath() && this.target.onHurt(this.atk);
            this.node.destroy(); 
            // 频繁创建销毁可使用对象池
        }
    }
}
