import Target from "./Target";

const ARR_SPEED = [1, 2, 4];

export default class BoatManager {
    private boats: Array<Target> = [];
    private bullets: cc.Node = null;
    private index: number = 0;

    public setBulletsNode(node: cc.Node) {
        this.bullets = node;
    }

    public createBoat(parent: cc.Node, index: number, isEmeny: boolean) {
        // let boat = ResManager.getInstance().getPrefab(isEmeny ? Prefabs.enemy : Prefabs.fleet);
        // boat.getComponent(Boat).initData(index);
        // boat.parent = parent;
        // this._boats.push(boat.getComponent(Boat));
    }

    // 目标选取规则：先打自己对位的，然后随机攻击
    public getShootTarget(wpos: cc.Vec2, atk: number, enemy: boolean, index: number) {
        // let Target: Target;
        // let arr = [];
        // let idxs = [];
        // this.boats.forEach(b => {
        //     if (!b.GetIsDeath && b.isEnemy() != enemy) {
        //         arr[b.getIndex()] = b;
        //         idxs.push(b.getIndex());
        //     }
        // });
        // if (idxs.indexOf(index) > -1) {
        //     boat = arr[index];
        // } else {
        //     boat = arr[idxs[Util.getRandomInt(0, idxs.length)]];
        // }
        // if (boat) {
        //     let bullet = ResManager.getInstance().getPrefab(Prefabs.bullet);
        //     bullet.parent = this._bullets;
        //     bullet.getComponent(Bullet).initBullet(atk, wpos.add(enemy ? cc.v2(-60, 20) : cc.v2(60, 20)), boat.node.convertToWorldSpaceAR(cc.Vec2.ZERO), boat);
        // }
    }

    /**
     * 设置战斗速度索引
     * @param index 
     */
    public setFightSpeedIndex(index: number) {
        this.index = index % ARR_SPEED.length;
    }

    /**获取战斗速度 */
    public getFightSpeed() {
        return ARR_SPEED[this.index];
    }

    private static _instance: BoatManager = null;
    public static getInstance(): BoatManager {
        if (!this._instance) {
            this._instance = new BoatManager();
        }
        return this._instance;
    }
}