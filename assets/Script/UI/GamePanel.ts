import UIParent from "../../FW/manager/UIParent";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GamePanel extends UIParent {

    async PanelInit(zIndex: number): Promise<void> {
        this.node.zIndex = zIndex
    }
    async PanelReset(zIndex: number): Promise<void> {
        this.node.zIndex = zIndex
        this.node.active = true
    }
    async PanelClose(callback: Function): Promise<void> {
        if(callback) callback()
        this.node.zIndex = 0
        this.node.active = false
        // this.node.addChild()

        let btn = new cc.Node().addComponent(cc.Button)
        btn.enableAutoGrayEffect    // 按钮置灰
        let spine = new cc.Node().addComponent(sp.Skeleton)
        spine.enabledInHierarchy = false

        // cc.js.formatStr
        // this.node.stopAllActions()
        // this.node.runAction(cc.repeatForever(cc.sequence(cc.delayTime(1), () => {})))
        // this.node.removeFromParent()
    }   

}
