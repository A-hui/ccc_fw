import UIParent from "../../FW/manager/UIParent";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MainPanel extends UIParent {

    async PanelInit(zIndex: number): Promise<void> {
        this.buttonEventAdd()
        this.node.zIndex = zIndex
    }
    async PanelReset(zIndex: number): Promise<void> {
        this.node.zIndex = zIndex
        this.node.active = true
    }
    async PanelClose(callback: Function): Promise<void> {
        this.node.zIndex = 0
        this.node.active = false
        if (callback) callback()
    }
    

    private buttonEventAdd() {
        
    }
}
