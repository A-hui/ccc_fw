import GameConfig from "../config/GameConfig"

/**
 * 游戏数据缓存类。
 * 本地数据与后端通信数据存储
 */
export default class GameData {
    /**
     * 游戏缓存数据
     */
    private static allData = {
        gameLevel: 1,
        judgeVibrate: 1,
        jdugeMusic: 1,
        OffLineTime: '',
    }
    /**游戏常数 */
    private static ConstData = {
        
    }
    /**
     * 获取游戏常用数据
     * @param name 数据名字
     */
    public static GetSingleData(name: string): number | string {
        if (typeof GameData.allData[name] == 'number' && isNaN(GameData.allData[name])) {
            console.log(name, '获取的数据为NAN，修改为0', GameData.allData[name])
            GameData.allData[name] = 0
        }
        if (GameData.allData[name] == null) {
            console.log(name, '获取的数据为空，返回0', GameData.allData[name])
            return 0
        }
        else return GameData.allData[name]
    }
    /**
     * 重置游戏常用数据
     * @param name  数据名字
     * @param data  最新数据
     */
    public static SetSingleData(name: string, data: any): void {
        if (GameData.allData[name] == null && name != 'currentPower') {
            console.error('name is not found!!check this name: ' + name)
            return
        }
        if (typeof data == 'number' && isNaN(data)) {
            console.error(name + "————data为空， 修改传入参数为0")
            data = 0
        }
        if (data < 0 && name == 'currentPower') {
            console.log('传入参数小于0，且为体力参数，修改传入参数为6')
            data = 6
        }
        GameData.allData[name] = data
    }
    /**
     * 获取静态数据
     * @param name 数据名称
     */
    public static GetCommonData(name: string): number {
        return GameData.ConstData[name] == null ? null : GameData.ConstData[name]
    }

    /** 保存游戏数据到 本地/服务端*/
    public static SaveData(): void {
        new Promise((resolve: Function, reject: Function) => {
            if (!GameConfig.isNetWork) {  // 无网络状态下存储数据到缓存
                cc.sys.localStorage.setItem(GameConfig.BaseDataKey, JSON.stringify(this.allData))
                resolve()
            } else {                      // 有网络下保存数据到服务器
                if (cc.sys.platform === cc.sys.WECHAT_GAME) { // 微信环境下

                } else if (cc.sys.platform === cc.sys.BYTEDANCE_GAME) { // 字节环境下

                }
            }
        })
    }

    /** 从本地/服务器获取数据 */
    public static GetData(callback?: Function) {
        new Promise((resolve: Function, reject: Function) => {
            if (!GameConfig.isNetWork) {    // 无网络状态下存储数据到缓存
                const localData = cc.sys.localStorage.getItem(GameConfig.BaseDataKey)
                if (localData) {    // 本地有数据
                    this.SetData(localData)
                } else {    // 本地无数据
                    this.SaveData()
                }
                resolve()
            } else {       // 有网络下保存数据到服务器
                if (cc.sys.platform === cc.sys.WECHAT_GAME) { // 微信环境下

                } else if (cc.sys.platform === cc.sys.BYTEDANCE_GAME) { // 字节环境下

                }
            }
        })
    }
    /**
     * 加载本地缓存的游戏数据
     */
    public static LoadDataByLocalStorage(): any {
        return cc.sys.localStorage.getItem(GameConfig.BaseDataKey)
    }
    /**
     * 更新游戏数据
     * @param data 本地缓存的数据
     * @param callBack 回调
     */
    public static SetData(data: any) {
        // return
        let json = JSON.parse(data)
        for (let key in json) {
            if (json[key] == null || json[key] == 'null') continue
            else
                GameData.SetSingleData(key, json[key])
        }
    }
    /**
     * 检测此数据是否存在
     * @param name 需检测的数据名字
     */
    public static ContainData(name: string): boolean {
        return GameData.allData[name] == null
    }
}