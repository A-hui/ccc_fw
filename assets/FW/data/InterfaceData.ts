/** 接口定义 */
export default class InterfaceData { }

/**
 * 场景信息
 */
 export interface sceneData {
    /** 场景存储路径 */
    path: string,
    /** 场景名字（标识）*/
    name: string,
    /** 场景对应类 */
    component: any
}
/**
 * 事件信息
 */
export interface EventData {
    /** 事件名字 */
    eventName: string,
    /** 事件函数 */
    eventFunc: Function,
    /** 是否调用一次后自动销毁事件 */
    isOnce: boolean,
}
/**
 * update循环事件信息
 */
export interface CycEvent {
    /** 事件名字（事件唯一标识）*/
    cycName: string,
    /** 事件（带参dt=>秒）*/
    cycFunc: Function
}
/**
 * 计时器事件信息
 */
export interface TimerEvent {
    /** 事件名字(事件唯一标识) */
    cycName: string,
    /** 事件(带参dt=>秒) */
    cycFunc: Function,
}
/**
 * 寻找子物体信息（返回类型默认Node）
 */
export interface FindComponentData {
    /** 父物体（从此开始循环寻找）*/
    parent: cc.Node,
    /** 进行搜索的路径 */
    path: string,
}
/**
 * 数字图集缓存
 */
export interface NumberAtlasCache {
    /** 名字 */
    name: string,
    /** 图集 */
    atlas: cc.SpriteAtlas
}
/**
 * 用户登录信息
 */
export interface LoginInfo {
    /** 用户openId */
    openId: string,
    /** 用户uToken */
    uToken: string
}
/**
 * 获取指定用户的指定排行榜所需传递的参数
 */
export interface rankGetParams {
    /**从/login获取的登录串uToken */
    uToken: string,
    /**榜单类型 */
    rankType: string,
    /**指定用户的openid */
    getOpenId: string
}
/**小游戏冷启动时返回的参数 */
export interface LaunchParams {
    /**启动小游戏的场景值 */
    scene: number,
    /**启动小游戏的 query 参数 */
    query: Object,
    /**shareTicket */
    shareTicket: string,
    /**来源信息。从另一个小程序、公众号或 App 进入小程序时返回。否则返回 {} */
    referrerInfo: referrerInfo
}
/**来源信息 */
export interface referrerInfo {
    /**来源小程序、公众号或 App 的 appId */
    appId: string,
    /**来源小程序传过来的数据，scene=1037或1038时支持 */
    extraData: object
}
/**友盟关卡中行为事件参数 */
export interface umaStageRunningparams {
    /**商品/道具名称 */
    itemName: string,
    /**商品/道具ID */
    itemId?: string,
    /**商品/道具数量 */
    itemCount?: string,
    /**商品/道具单价 */
    itemMoney?: string,
    /**描述 */
    desc?: string,
}