
/**
 * 游戏逻辑常用数据
 * 游戏逻辑常用接口
 */
export default class CommonData {

}
/**小程序id */
export const AppID = []

/**
 * 图集名字
 */
export const AtlasName = {}
/**
 * AB包名字
 */
export const ABName = {
    CommonUse: 'CommonUse'
}
/**
 * 音效名字
 */
export const SourceName = {
    /**背景音乐 */
    BGM: 'bgm',
}
/**
 * 文件后缀名
 */
export const Postfix = {

}
/**
 * 预制体名字
 */
export const PrefabName = {
}
/**
 * 动画名称
 */
export const AniName = {
}
/**骨骼信息数据 */
export const DragonBonesAssetName = {
   
}
/**骨骼纹理数据 */
export const DragonBonesAtlasAssetName = {
    
}
/**
 * 数字特殊符号
 */
export const NumberToStr = {
    '/': 'xg',
    '-': 'hg',
    '关': 'guan',
    '秒': 'miao',
    '第': 'di',
    '级': 'lv',
    '.': 'dian',
    '%': '%',
    '伤': 'ms',
    '+': 'jia',
    ':': 'maohao',
    'x': 'x',
}
/**
 * 广告名字
 */
export const ADSName = {
   
}

export enum GamePattern {
    common = 0,
    day_challenge = 1,
    challenge = 2,
}