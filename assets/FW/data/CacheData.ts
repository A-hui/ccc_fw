/**缓存数据, 只作为单次游戏的缓存，不保存到数据库的数据 */
export default class CacheData {
    /**渠道登录ID */
    public static playerToken = {
        wxOpenId: ''
    }

    /**是否已经初始化过数据 */
    public static isDataInit = false

    /**最近一次离线时间(秒) */
    public static offLineSeconds = 0 

    /**是否首次(loading进入游戏)加载游戏 ---→ (判断是否加载健康忠告)  */
    public static isFirstLoading = true

    /**下一个场景(loading后跳转) */
    public static  nextScene = ''
}