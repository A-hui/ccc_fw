
/**
 * 场景常用信息
 */
export const Scene_Data = {
    loading: {
        name: '',
        path: '',
        component: ''
    },
    GamePanel: {
        name: 'GamePanel',
        path: 'GamePanel',
        component: 'GamePanel'
    },
    MainPanel: {
        name: 'MainPanel',
        path: 'MainPanel',
        component: 'MainPanel'
    }
}