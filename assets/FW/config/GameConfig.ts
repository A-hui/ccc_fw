export default class GameConfig {

    /**是否有网络，有网络时会发起短连接，无网络时数据则保存在缓存中 */
    public static isNetWork = false

    /**是否调试模式，显示调试工具 */
    public static isDebug = true

    /**loading 完成后要加载的场景名称 */
    public static FirstScene = 'game'

    /**储存数据的 key，任意的字符串，建议写复杂一些不容易与其他 key 冲突 */
    public static BaseDataKey = '_example$%*'
}


/**健康忠告 */
export const HealthAdvice = {
    /**是否显示 */
    isShow: true,
    /**显示时间/秒 */
    showTimeS: 2,
}

/**loading设置 */
export const Loading = {
    /**是否延时 loading */
    isDealy: true,
    /**延时的时间 / 秒 */
    dealyTimeS: 1,
}

/**体力 */
export const Power = {
    /**是否开启体力值 */
    isAble: true, // 
    /**初始值 */
    originalValue: 3,
    /**体力值上限 */
    upperLimit: 20,
    /**体力是否能突破上限 */
    canBreakLimit: true,
    /**体力恢复周期 / 秒 */
    recoverCycleS: 1 * 60,
    /**每个周期恢复的数值 */
    recoverAmount: 1,
}

/**音效 */
export const Sound = {
    bgmUrl: 'sounds/background_music',
    /**背景音乐音量 */
    bgmVolume: 0.6,
    /**音效音量 */
    effectVolume: 1,
}

/**设计分辨率，适配用 */
export const DesignResolution = {
    /**高 */
    height: 1334,
    /**宽 */
    width: 750
}