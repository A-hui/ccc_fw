
cc.Class({
    extends: cc.Component,
    onLoad() {
        let c = this.node.getComponent(cc.Canvas);
        c.fitHeight = true;
        c.fitWidth = false;
        let h = 750 * cc.winSize['height'] / cc.winSize['width'];
        c.designResolution = new cc.Size(750, h);
        this.node.setContentSize(750, h);
    },
});
