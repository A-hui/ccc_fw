
export default class PoolManager {
    private static poolDic = new Object() as { string: cc.NodePool }
    /**
     * 初始化对象池
     * @param pool_name 对象池名字
     * @param component 对象池组件
     */
    public static InitPool(pool_name: string, component: any): void {
        if (PoolManager.poolDic[pool_name] == null) {
            let pool = new cc.NodePool(component)
            PoolManager.poolDic[pool_name] = pool
        }
    }
    /**
     * 获取对象池内对象，空则返回空
     * @param pool_name 对象池名字
     */
    public static GetObj(pool_name: string): cc.Node {
        if (PoolManager.poolDic[pool_name] == null) {
            console.error('pool is empty')
            return null
        }
        return (PoolManager.poolDic[pool_name] as cc.NodePool).get()
    }
    /**
     * 回收对象
     * @param pool_name 对象池名字
     */
    public static PutObj(pool_name: string, obj: cc.Node, ObjInit: Function): void {
        if (PoolManager.poolDic[pool_name] == null) {
            console.error('pool is empty')
            return null
        }
        if (ObjInit != null)
            ObjInit();
        (PoolManager.poolDic[pool_name] as cc.NodePool).put(obj)
    }
    public static GetPoolCount(pool_name: string): number {
        if (PoolManager.poolDic[pool_name] == null) {
            console.error('pool is empty')
            return null
        }
        return PoolManager.poolDic[pool_name].count
    }
}
