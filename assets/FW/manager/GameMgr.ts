import UIManager from './UIManager';
import { ABName, SourceName, ADSName, AtlasName, GamePattern } from '../data/CommonData';
import GameData from '../data/GameData';
import PoolManager from './PoolManager';
import ComonTools from '../tools/ComonTools';
import LongTimeCountDown from '../tools/LongTimeCountDown';
import { Scene_Data } from '../data/SceneData';
import EventManager from './EventManager';
import WXSDK from '../sdk/WXSDK';
import { CycEvent, FindComponentData, NumberAtlasCache, TimerEvent } from '../data/InterfaceData';

const { ccclass } = cc._decorator;

@ccclass
export default class GameMgr extends cc.Component {
    /**
     * @param update事件数组
     */
    private static CycDic: Array<CycEvent>
    /**
     * @param 资源缓存集合
     */
    private static resCacheDic: { string: any }
    /**
     * @param 计时器集合
     */
    private static TimerDic: Array<TimerEvent>
    /**
     * @param AB包缓存集合
     */
    private static ABCacheDis: { string: cc.AssetManager.Bundle }
    /**
     * @param 数字图集队列
     */
    private static NumberAtlasArr: Array<NumberAtlasCache>
    /**@param 玩家头像图片缓存 */
    public static playerTexture: cc.Texture2D
    public static _GamePattern: GamePattern = GamePattern.common
    public static _watchADSLevel = 1
    public static tip_banner: any
    public static LongTimeCountDown: LongTimeCountDown
    public static GridADSNone = false
    public static shareOutTime = null


    /**
     * 游戏管理类初始化
     */
    public static GameInit() {
        GameMgr.CycDic = new Array<CycEvent>()
        GameMgr.TimerDic = new Array<TimerEvent>()
        GameMgr.NumberAtlasArr = new Array<NumberAtlasCache>()
        GameMgr.resCacheDic = new Object() as { string: any }
        GameMgr.ABCacheDis = new Object() as { string: cc.AssetManager.Bundle }
        PoolManager.InitPool('loadingWait', cc.Node)
        PoolManager.InitPool('number', cc.Sprite)
        PoolManager.InitPool('commonCountDown', cc.Node)
        PoolManager.InitPool('commonGift', cc.Node)
        PoolManager.InitPool('jump', cc.Button)
        UIManager.Init()
        GameMgr.OnShow()
        GameMgr.OnHide()
        GameMgr.GetLaunchOptionsSync()
    }
    update(dt: number) {
        if (GameMgr.CycDic == null) return
        if (GameMgr.CycDic.length == 0) return
        for (let i = 0; i < GameMgr.CycDic.length; i++) {
            if (GameMgr.CycDic[i] == null) continue
            GameMgr.CycDic[i].cycFunc(dt)
        }
    }


    /**获取小游戏冷启动时的参数 */
    public static GetLaunchOptionsSync() {
        let res = WXSDK.getLaunchOptionsSync()
        if (res) {
            if (res.scene == 1011 || res.scene == 1030 || res.scene == 1047) {

            } else if (res.scene == 1095 || res.scene == 1037) {
            
            } else {
            }
        }
        console.log('冷启动参数', res)
    }
    /**游戏回到前台 */
    public static OnShow(): void {
        WXSDK.OnShow((res) => {
            console.log('onShow',JSON.stringify(res))
            let data = GameData.LoadDataByLocalStorage()
            if (data != null && data != '')
                GameData.SetData(data)
            GameMgr.OffLineTime()
        })
    }
    /**游戏返回后台 */
    public static OnHide(): void {
        WXSDK.OnHide(() => {
            let HideTime = new Date()
            let str = (HideTime.getMonth() + 1) + '/' + HideTime.getDate() + '/' + HideTime.getHours() + '/' + HideTime.getMinutes()
            console.log('OffLineTime = ' + str)
            GameData.SetSingleData('OffLineTime', str)
            // GameData.SaveDataToServer()
            GameData.SaveData()
        })
    }
    /**
     * 打开子域
     */
    public static SubContextShow(data: any, event: string, judgeOpenPanel: boolean) {
        let Time = new Date().getTime()
        if (judgeOpenPanel) {
            // UIManager.OpenPanel(Scene_Data.RankingPanel, true, () => {
            //     EventManager.EventPlay('RankingOpenAni')
            // })
        }
        // 获取排行榜上对应的信息
        WXSDK.OpenDataContext('GET', data, event, Time)
        let sub = cc.find('game_Canvas/subContext').getComponent(cc.SubContextView)
        // openDataContext.width = sub.node.parent.width
        // openDataContext.height = sub.node.parent.height
        sub.node.zIndex = 999
        sub.node.active = true
        sub.enabled = true
        sub.updateSubContextViewport()
    }
    /**
     * 隐藏子域
     */
    public static HideSubContext() {
        let sub = cc.find('game_Canvas/subContext').getComponent(cc.SubContextView)
        sub.node.zIndex = 0
        sub.node.active = false
        WXSDK.OpenDataContext('DEL', null, 'removeData', new Date().getTime() / 1000)
    }
    /**
     * 获取离线收益
     */
    public static OffLineTime() {
        let hideStr = GameData.GetSingleData('OffLineTime') as string
        let currentPower = GameData.GetSingleData('currentPower') as number
        let maxPower = GameData.GetCommonData('recoverMaxPower')
        console.log("back hideStr = " + hideStr)
        if (hideStr == null || hideStr == '') return
        let showTime = new Date()
        let show_hours = showTime.getHours()
        let show_min = showTime.getMinutes()
        let show_day = showTime.getDate()
        let show_month = showTime.getMonth() + 1
        let hide_month = 0, hide_day = 0, hide_hours = 0, hide_min = 0
        let str_arr = hideStr.split('/')
        hide_month = parseInt(str_arr[0])
        hide_day = parseInt(str_arr[1])
        hide_hours = parseInt(str_arr[2])
        hide_min = parseInt(str_arr[3])
    
        if (show_month != hide_month) {//月份不同逻辑
            if (currentPower < maxPower) {
               
            }
            GameData.SaveData()
            return
        }
        if (show_day != hide_day) {
            if (show_hours < hide_hours)
                show_hours += 24 * (show_day - hide_day)
            if (currentPower < maxPower) {
                GameData.SetSingleData('currentPower', maxPower)
            }
            // 重置登录领取奖励次数
            GameData.SetSingleData('AwardCount', 0)
        }
        let hours = show_hours - hide_hours
        if (hours >= 2) {//离线大于2小时逻辑
            if (currentPower < maxPower) {
                GameData.SetSingleData('currentPower', maxPower)
            }
        } else {
            let min = 0
            if (show_min < hide_min) {
                let hours = show_hours - hide_hours - 1
                min = hours * 60 + (60 - (hide_min - show_min))
            } else {
                min = hours * 60 + (show_min - hide_min)
            }
            if (min >= 90) {//大于150分钟逻辑
                if (currentPower < maxPower)
                    GameData.SetSingleData('currentPower', maxPower)
            } else {//其他
                if (currentPower >= maxPower) return
                let power = Math.floor(min / 10)
                if (currentPower + power >= GameData.GetCommonData('maxPower')) {
                    GameData.SetSingleData('currentPower', maxPower)
                }
                else {
                    GameData.SetSingleData('currentPower', currentPower + power)
                }
            }
        }
        GameData.SaveData()
    }
    /**
     * 计时器（如果名字相同，则不能取消定时器内方法，需谨慎）
     * @param timerEvent 计时器所需参数
     * @param component 组件
     * @param interval 计时时间(0表示下一帧)
     * @param repeat 重复次数(cc.macro.REPEAT_FOREVER为无限)
     * @param delay 延迟执行时间
     */
    public static Timer(timerEvent: TimerEvent, component: cc.Component, interval = 0.02, repeat = 0, delay = 0): void {
        if (GameMgr.TimerDic.length == 0) {
            GameMgr.TimerDic[0] = timerEvent
            component.schedule(GameMgr.TimerDic[0].cycFunc, interval, repeat, delay)
        }
        for (let i = 0; i < GameMgr.TimerDic.length; i++) {
            if (GameMgr.TimerDic[i].cycName == timerEvent.cycName) {
                GameMgr.TimerDic[i] = timerEvent
                component.schedule(GameMgr.TimerDic[i].cycFunc, interval, repeat, delay)
                return
            }
        }
        GameMgr.TimerDic.push(timerEvent)
        component.schedule(timerEvent.cycFunc, interval, repeat, delay)
    }
    /**
     * 取消定时器内方法
     * @param component 组件
     * @param cycName 计时器名字
     */
    public static UnTimer(component: cc.Component, cycName: string): void {
        if (GameMgr.TimerDic.length == 0) return
        for (let i = 0; i < GameMgr.TimerDic.length; i++) {
            if (GameMgr.TimerDic[i].cycName == cycName) {
                component.unschedule(GameMgr.TimerDic[i].cycFunc)
                cc.js.array.removeAt(GameMgr.TimerDic, i)
                return
            }
        }
    }
    /**
     * 寻找场景内物体
     * @param componentData 物体信息与地址
     */
    public static FindComponent(componentData: FindComponentData): cc.Node {
        let path_arr = componentData.path.split('/')
        let temp_obj = componentData.parent
        for (let i = 0; i < path_arr.length; i++) {
            temp_obj = temp_obj.getChildByName(path_arr[i])
        }
        return temp_obj
    }
    /**
     * 按钮事件绑定。可控制按钮是否只能点击一次，通过回调恢复按钮点击功能
     * @param button 绑定事件的按钮
     * @param eventFun 绑定到按钮上的事件（默认带两参：event=>按钮点击返回参数，callback=>恢复按钮点击功能）
     * @param isHide 是否点击按钮后隐藏按钮（默认为true）
     */
    public static ButtonEventBinding(button: any, type: string, eventFun: Function, isHide = true, isGive = false): void {
        button.node.on(type, (event: cc.Event.EventTouch) => {
            if (button.interactable == false)
                return
            // if (!isGive)
            //     ComonTools.PlaySource(SourceName.tap)
            // else
            // ComonTools.PlaySource(SourceName.click)
            if (isHide)
                button.interactable = false
            eventFun(event, () => {
                button.interactable = true
            })
        })
    }
    /**
     * 后台加载AB包
     * @param ABName ab包名字
     * @param callback 回调
     */
    public static PreLoadABPack(ABName: string, callback?: Function) {
        cc.assetManager.loadBundle(ABName, (err, res: cc.AssetManager.Bundle) => {//
            if (err != null) {
                console.error(err)
                return
            }
            GameMgr.SetABCacheDis(ABName, res)
            if (callback != null)
                callback()
        })
    }
    /**
     * 后台加载AB包内资源
     * @param ABName ab包名字
     * @param path 包内资源地址
     * @param resName 资源缓存名字
     * @param type 资源类型
     * @param callback 回调
     */
    public static PreLoadResDirBack(ABName: string, path: string, resName: string, type: typeof cc.Asset, callback?: Function) {
        let ab: cc.AssetManager.Bundle = GameMgr.ABCacheDis[ABName]
        if (ab == null) {
            console.error('ab is null,please check the name!!!!!!!!' + ABName)
            return
        }
        ab.loadDir(path, type, (err, res) => {
            if (err != null) {
                console.error(err)
                return
            }
            for (let i = 0; i < res.length; i++)
                GameMgr.resCacheDic[resName + res[i].name] = res[i]
            if (callback != null) callback(res)
        })
    }
    /**
     * 新增数字图集缓存
     * @param path 地址
     * @param name 缓存名字
     */
    public static async SetNumberAtlasCache(path: string, name: string): Promise<void> {
        let ab = await GameMgr.GetBundle(ABName.CommonUse)
        let temp_atlas: cc.SpriteAtlas = await new Promise<cc.SpriteAtlas>(resolve => {
            ab.load(path, cc.SpriteAtlas, (err, res: cc.SpriteAtlas) => {
                if (err != null) {
                    console.error(err)
                    return null
                }
                return resolve(res)
            })
        })
        GameMgr.NumberAtlasArr.push({ name: name, atlas: temp_atlas })
    }
    /**
     * 获取数字图集缓存
     * @param name 缓存名字
     */
    public static GetNumberAtlasCache(name: string): cc.SpriteAtlas {
        for (let i = 0; i < GameMgr.NumberAtlasArr.length; i++) {
            if (GameMgr.NumberAtlasArr[i].name == name)
                return GameMgr.NumberAtlasArr[i].atlas
        }
        return null
    }
    public static TempSetNumberAtlasCache(name: string, atlas: cc.SpriteAtlas) {
        GameMgr.NumberAtlasArr.push({ name: name, atlas: atlas })
    }
    /**
     * 判断缓存中是否有此图集
     * @param name 缓存名字
     */
    public static ContainsNameInNumberAtlas(name: string): boolean {
        for (let i = 0; i < GameMgr.NumberAtlasArr.length; i++) {
            if (GameMgr.NumberAtlasArr[i].name == name)
                return true
        }
        return false
    }
    /**
     * 获取音效文件
     * @param clipName 音效名字
     */
    public static async GetAudioClip(clipName: string): Promise<cc.AudioClip> {
        let allClip = GameMgr.GetRes<cc.AudioClip[]>('allSources')
        if (allClip == null) {
            GameMgr.SetResCacheDic('allSources', await GameMgr.ResLoadDir(ABName.CommonUse, 'allSources', cc.AudioClip))
            allClip = GameMgr.GetRes<cc.AudioClip[]>('allSources')
        }
        for (let i = 0; i < allClip.length; i++) {
            if (allClip[i].name == clipName)
                return allClip[i]
        }
        return null
    }
    /**
     * 获取缓存资源
     * @param name 资源名称
     */
    public static GetRes<T>(name: string): T {
        return GameMgr.resCacheDic[name]
    }
    /**
     * 获取路径下的所有资源中的单独资源
     * @param dirName 路径名字
     * @param resName 资源名字
     */
    public static GetResByDir<T>(dirName: string, resName: string): T {
        for (let i = 0; i < GameMgr.resCacheDic[dirName].length; i++) {
            if (GameMgr.resCacheDic[dirName][i].name == resName)
                return GameMgr.resCacheDic[dirName][i]
        }
    }
    /**
     * 移除缓存中的AB包名字
     * @param name AB包名字
     */
    public static RemoveABPack(name: string) {
        if (GameMgr.ABCacheDis[name] == null) return
        cc.assetManager.removeBundle(GameMgr.ABCacheDis[name])
        GameMgr.ABCacheDis[name] = null
    }
    /**
     * 加载AB包内的资源
     * @param ABName AB包名字
     * @param path 包内资源地址
     * @param type 资源类型
     */
    public static async ResLoadByAB(ABName: string, path: string, type: typeof cc.Asset): Promise<any> {
        let ab = await GameMgr.GetBundle(ABName)
        return new Promise<any>(resolve => {
            ab.load(path, type, (err, res) => {
                if (err != null) {
                    console.error(err)
                    return null
                }
                return resolve(res)
            })
        })
    }
    /**
     * 通过AB包加载资源路径下的全部资源
     * @param dir 资源总路径
     * @param type 类型
     * @param bundleName AB包名字
     * @param callback 回调
     */
    public static async ResLoadDir(ABName: string, dir: string, type: typeof cc.Asset): Promise<any> {
        let ab = await GameMgr.GetBundle(ABName)
        return new Promise<any>(resolve => {
            ab.loadDir(dir, type, (err, res) => {
                if (err != null) {
                    console.error(err)
                    return null
                }
                return resolve(res)
            })
        })
    }
    /**
     * AB包加载
     * @param ABName AB包名字
     */
    public static async ABLoad(ABName: string): Promise<cc.AssetManager.Bundle> {
        return new Promise<cc.AssetManager.Bundle>(resolve => {
            cc.assetManager.loadBundle(ABName, null, (err, res) => {
                if (err != null) {
                    console.log(err)
                    return null
                }
                return resolve(res)
            })
        })
    }
    /**
     * 预加载资源
     * @param dir 资源路径
     * @param type 类型
     * @param bundleName AB包名字
     * @param isDir 是否加载路径下全部资源
     */
    public static async ResPreLoad(ABName: string, dir: string, type: typeof cc.Asset, isDir: boolean): Promise<void> {
        let ab = await GameMgr.GetBundle(ABName)
        if (!isDir)
            ab.preload(dir, type)
        else
            ab.preloadDir(dir, type)
    }
    /**
     * 缓存res加载的资源
     * @param name 资源名称
     * @param res 资源
     */
    public static SetResCacheDic(name: string, res: any) {
        GameMgr.resCacheDic[name] = res
    }
    /**
     * 缓存AB包
     * @param name AB包名字
     * @param res AB包
     */
    public static SetABCacheDis(name: string, res: cc.AssetManager.Bundle) {
        GameMgr.ABCacheDis[name] = res
    }
    /**
     * 后台加载AB包资源
     * @param ABName AB包名字
     * @param resName 资源名字
     * @param path 地址
     * @param type 类型
     */
    public static ABLoadResBack(ABName: string, resName: string, path: string, type: typeof cc.Asset, isDir = false, isDis = false) {
        let ab: cc.AssetManager.Bundle = GameMgr.ABCacheDis[ABName]
        if (!isDir) {
            ab.load(path, type, (err, res) => {
                if (err != null) {
                    console.error(ABName, resName, 'ABLoadResBack', err)
                    return
                }
                GameMgr.SetResCacheDic(resName, res)
                if (isDis) {
                    cc.assetManager.removeBundle(GameMgr.ABCacheDis[ABName])
                    delete GameMgr.ABCacheDis[ABName]
                }
            })
        } else {
            ab.loadDir(path, type, (err, res) => {
                if (err != null) {
                    console.error(err)
                    return
                }
                GameMgr.SetResCacheDic(resName, res)
                if (isDis) {
                    cc.assetManager.removeBundle(GameMgr.ABCacheDis[ABName])
                    delete GameMgr.ABCacheDis[ABName]
                }
            })
        }
    }
    /**
     * 获取AB包
     * @param name AB包名字
     */
    public static async GetBundle(name: string): Promise<cc.AssetManager.Bundle> {
        if (GameMgr.ABCacheDis[name] == null) {
            GameMgr.SetABCacheDis(name, await GameMgr.ABLoad(name))
            return GameMgr.ABCacheDis[name]
        } else {
            return GameMgr.ABCacheDis[name]
        }
    }
    /**
     * 插入事件到游戏upate循环中
     * 一个Name对应一个函数，名字相同，则覆盖
     * @param cycData 需要循环的游戏数据
     */
    public static CycleEventPush(cycData: CycEvent): void {
        if (GameMgr.CycDic.length == 0) {
            GameMgr.CycDic[0] = cycData
            return
        }
        for (let i = 0; i < GameMgr.CycDic.length; i++) {
            if (GameMgr.CycDic[i].cycName == cycData.cycName) {
                GameMgr.CycDic[i] = cycData
                return
            }
        }
        GameMgr.CycDic.push(cycData)
    }
    /**
     * 根据名字，移除事件方法
     * @param cycName 事件对应的名字
     */
    public static CycleEventRemove(cycName: string): void {
        if (GameMgr.CycDic.length == 0) return
        for (let i = 0; i < GameMgr.CycDic.length; i++) {
            if (GameMgr.CycDic[i].cycName == cycName) {
                cc.js.array.removeAt(GameMgr.CycDic, i)
                break
            }
        }
    }
    /**
     * 根据名字判断是否存在此事件
     * @param cycName 事件对应名字
     * @returns true:存在；false:不存在
     */
    public static ContainsKeyInCycle(cycName: string): boolean {
        if (GameMgr.CycDic.length == 0) return false
        for (let i = 0; i < GameMgr.CycDic.length; i++)
            if (GameMgr.CycDic[i].cycName == cycName)
                return true
        return false
    }
    /**
     * 根据名字判断计时器集合中是否有此事件
     * @param cycName 计时器名字
     * @returns ture：存在，false：不存在
     */
    public static ContainsKeyInTimer(cycName: string): boolean {
        if (GameMgr.TimerDic.length == 0) return false
        for (let i = 0; i < GameMgr.TimerDic.length; i++) {
            if (GameMgr.TimerDic[i].cycName == cycName)
                return true
        }
        return false
    }
}

