import UIParent from './UIParent';
import { sceneData } from '../data/InterfaceData'
import GameMgr from './GameMgr';

export default class UIManager {
    private static allScene: { string: UIParent }
    private static scene_zInder: number
    private static sceneParent: cc.Node
    /**
     * 初始化管理器
     */
    public static Init(): void {
        UIManager.allScene = new Object() as { string: UIParent }
        UIManager.scene_zInder = 0
    }
    /**
     * 显示已加载的画面，未加载的场景会返回空
     * @param name 场景名字
     * @param callback 成功打开场景后回调
     * @param aniFun 场景显示动画
     * @param callbackPara 回调参数
     */
    public static async ShowPanel(name: string, callback?: Function, aniFun?: Function, ...callbackPara: any): Promise<void> {
        let tempScene: UIParent = UIManager.allScene[name]
        if (tempScene == null) {
            console.error('scene is null,check the name!!!!!!!!!!!!!!!!!!' + name)
            return
        }
        if (this.sceneParent == null)
            this.sceneParent = cc.find('game_Canvas')
        tempScene.node.parent = this.sceneParent
        tempScene.node.name = name
        await tempScene.PanelInit(UIManager.scene_zInder * 10)
        tempScene.node.active = true
        if (callback != null)
            callback(...callbackPara)
        if (aniFun != null)
            aniFun()
    }
    /**
     * 打开界面，首次打开调用Init方法，往后调用Reset方法
     * @param isShow 是否显示画面，默认为true，当需要暂缓展示，则可以设置为false
     * @param sceneData 场景常用信息
     * @param callback 回调
     * @param aniFun 动画事件
     * @param callbackPara 回调参数 
     */
    public static async OpenPanel(sceneData: sceneData, isShow = true, callback?: Function, aniFun?: Function, ...callbackPara: any): Promise<void> {
        if (UIManager.allScene[sceneData.name] == null) {
            let canvas = await GameMgr.ResLoadByAB(sceneData.name, sceneData.path, cc.Prefab)
            let UIPrefab = cc.instantiate(canvas)
            let UIComponent: UIParent = UIPrefab.getComponent(sceneData.component)
            if (UIComponent == null) {
                UIComponent = UIPrefab.addComponent(sceneData.component)
            }
            UIManager.scene_zInder += 1
            UIManager.allScene[sceneData.name] = UIComponent
            if (isShow)
                UIManager.ShowPanel(sceneData.name, callback, aniFun, callbackPara)
            else {
                if (callback != null) callback()
            }
        } else {
            UIManager.ResetPanel(sceneData, callback, aniFun, callbackPara)
        }
    }
    /**
     * 重置界面，调用Reset方法
     * @param sceneData 场景常用信息
     * @param callback 回调
     * @param callbackPara 回调参数
     */
    public static async ResetPanel(sceneData: sceneData, callback?: Function, aniFun?: Function, ...callbackPara: any): Promise<void> {
        UIManager.scene_zInder += 1
        await UIManager.allScene[sceneData.name].PanelReset(UIManager.scene_zInder * 10)
        if (callback != null) callback(...callbackPara)
        if (aniFun != null) aniFun()
    }
    /**
     * 关闭界面，调用close方法
     * @param sceneData 场景常用信息
     * @param callback 回调
     */
    public static async ClosePanel(sceneData: sceneData, callback: Function): Promise<void> {
        if (UIManager.allScene[sceneData.name] == null) return
        await UIManager.allScene[sceneData.name].PanelClose(callback)
    }
    /**
     * 通过场景名字获取场景对象
     * @param sceneName 场景名字
     */
    public static GetSceneComponent<T>(sceneName: string): T {
        if (UIManager.allScene[sceneName] == null) {
            console.error('scene is not creat,please check the name!!' + sceneName)
            return null
        }
        return UIManager.allScene[sceneName]
    }
}
