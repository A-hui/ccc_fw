const { ccclass } = cc._decorator;

@ccclass
export default abstract class UIParent extends cc.Component {
    abstract PanelInit(zIndex: number): Promise<void>
    abstract PanelReset(zIndex: number): Promise<void>
    abstract PanelClose(callback: Function): Promise<void>
}