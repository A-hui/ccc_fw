
import { NumberToStr, PrefabName, SourceName, ABName } from '../data/CommonData';
import EventManager from "../manager/EventManager"
import GameData from '../data/GameData';
import GameMgr from '../manager/GameMgr';
import PoolManager from '../manager/PoolManager';

export default class ComonTools {
    /**
     * 根据坐标判断距离
     * @param origin 开始位置
     * @param target 结束位置
     * @param judge_dis 目标距离
     * @returns true为到达目标距离，false为未到达
     */
    public static JudgeDistance(origin: cc.Vec2, target: cc.Vec2, judge_dis: number): boolean;
    public static JudgeDistance(origin: cc.Vec2, target: cc.Vec2, judge_dis: cc.Vec2): boolean;
    /**
     * 根据坐标判断距离
     * @param origin 开始位置
     * @param target 结束位置
     * @param judge_dis 目标距离
     * @returns true为到达目标距离，false为未到达
     */
    public static JudgeDistance(origin: number, target: number, judge_dis: number): boolean;
    /**
     * 根据坐标判断距离
     * @param origin 开始位置
     * @param target 结束位置
     * @returns 整数
     */
    public static JudgeDistance(origin: cc.Vec2, target: cc.Vec2): number;
    /**
     * 根据坐标判断距离
     * @param origin 开始位置
     * @param target 结束位置
     * @returns 整数
     */
    public static JudgeDistance(origin: number, target: number): number;


    public static JudgeDistance(origin: cc.Vec2 | number, target: cc.Vec2 | number, judge_dis?: cc.Vec2 | number): boolean | number {
        if (typeof origin === "object" && typeof target === "object") {
            if (judge_dis != null) {
                let dis = origin.sub(target).mag()
                if (typeof judge_dis == 'object') {
                    return Math.abs(origin.x - target.x) <= judge_dis.x && Math.abs(origin.y - target.y) < judge_dis.y
                } else {
                    return dis <= judge_dis
                }
            } else {
                return Math.round(origin.sub(target).mag())
            }
        } else if (typeof origin === "number" && typeof target === "number") {
            if (judge_dis != null) {
                let dis = Math.abs(origin - target)
                return dis <= judge_dis
            } else {
                return Math.abs(origin - target)
            }
        }
    }
    /**使用希尔算法重排序 */
    public static XierSort(arr: any[]): any[] {
        for (let incre = arr.length / 2; incre > 0; incre /= 2) {
            incre = Math.floor(incre)
            for (let i = incre; i < arr.length; i++) {
                let j = i
                while (j - incre >= 0 && parseInt(arr[j].y) > parseInt(arr[j - incre].y)) {
                    let temp = arr[j]
                    arr[j] = arr[j - incre]
                    arr[j - incre] = temp
                    j -= incre
                }
            }
        }
        return arr
    }
    /**随机打乱数组(直接修改原数组) */
    public static shuffleArray(array: Array<any>) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }
    /**
     * 设置BGM音量
     */
    public static SetBGMValue(volume: number) {
        cc.audioEngine.setMusicVolume(volume)
    }
    /**
     * 设置生效音量
     */
    public static SetSoundValue(volume: number) {
        cc.audioEngine.setEffectsVolume(volume)
    }
    /**
     * 播放BGM
     * @param clipName 音效名字
     * @param loop 是否循环
     */
    public static async PlayBGM(ABName: string, clipName: string, loop = true): Promise<void> {
        let judgeMusic = GameData.GetSingleData('jdugeMusic') as number
        let level = GameData.GetSingleData('selectLevel') as number
        if (judgeMusic == 0 || level < 3) return
        let clip = GameMgr.GetRes<cc.AudioClip>(clipName)
        if (clip == null) {
            GameMgr.SetResCacheDic(clipName, await GameMgr.ResLoadByAB(ABName, clipName, cc.AudioClip))
            cc.audioEngine.playMusic(GameMgr.GetRes<cc.AudioClip>(clipName), loop)
        } else {
            cc.audioEngine.playMusic(clip, loop)
        }
    }
    /**
     * 停止播放BGM
     */
    public static StopBGm() {
        cc.audioEngine.stopMusic()
    }
    /**
     * 播放音效
     * @param clipName 音效名字
     * @param loop 是否循环
     */
    public static async PlaySource(clipName: string, loop = false) {
        let judgeMusic = GameData.GetSingleData('jdugeMusic') as number
        let level = GameData.GetSingleData('selectLevel') as number
        if (judgeMusic == 0 || level < 3) return
        cc.audioEngine.playEffect(await GameMgr.GetAudioClip(clipName), loop)
    }
    /**
     * 停止播放指定音效
     * @param sourceIndex 音乐下标
     */
    public static StopSource(sourceIndex: number) {
        cc.audioEngine.stopEffect(sourceIndex)
    }
    /**
     * 停止所有音效
     */
    public static StopAllSouces() {
        cc.audioEngine.stopAllEffects()
    }
    /**
     * 目标移动到下一个地点
     * @param obj 目标
     * @param target 下一个地点
     * @param speed 速度
     */
    public static BulletTrace(obj: cc.Node, target: cc.Vec2, speed: number) {
        let delta = target.sub(obj.getPosition())
        let x2 = obj.x + delta.x / delta.mag() * speed
        let y2 = obj.y + delta.y / delta.mag() * speed
        let deltaRotation = 90 - Math.atan2(x2 - obj.x, y2 - obj.y) * 180 / Math.PI
        obj.angle = deltaRotation
        obj.setPosition(x2, y2)

        // let deltaRotation = 90 - Math.atan2(x2 - obj.x, y2 - obj.y) * 180 / Math.PI
        // obj.angle = deltaRotation
        // obj.setPosition(x2, y2)
    }
    /**
     * 移动距离计算
     * @param obj 起点坐标
     * @param target 终点坐标
     * @param speed 速度
     * @returns 
     */
    public static DistTrace(obj: cc.Vec2, target: cc.Vec2, speed: number) {
        let delta = target.sub(obj)
        delta.x = delta.x / delta.mag() * speed
        delta.y = delta.y / delta.mag() * speed
        return delta
    }

    /**
     * 目标移动到下一个地点（带旋转）
     * @param obj 目标
     * @param target 下一个地点
     * @param speed 速度
     * @param isOpposite 是否相反（true：是的，false：不是）
     */
    public static BulletTraceWithRotation(obj: cc.Node, target: cc.Vec2, speed: number, isOpposite = false): void {
        let delta = target.sub(obj.getPosition())
        let x2 = obj.x + delta.x / delta.mag() * speed
        let y2 = obj.y + delta.y / delta.mag() * speed
        let deltaRotation = -(Math.atan2(x2 - obj.x, y2 - obj.y) * 180 / Math.PI)
        if (!isOpposite)
            obj.angle = deltaRotation
        else
            obj.angle = deltaRotation - 180
        obj.setPosition(x2, y2)
    }
    /**
     * 获取两坐标间坐标
     * @param startPos 开始位置
     * @param endPos 结束位置
     */
    public static GetAngle(startPos: cc.Vec2, endPos: cc.Vec2): number {
        let delta = endPos.sub(startPos)
        let x2 = startPos.x + delta.x / delta.mag()
        let y2 = startPos.y + delta.y / delta.mag()
        return 360 - Math.atan2(x2 - startPos.x, y2 - startPos.y) * 180 / Math.PI
    }
    /**
     * 获取点到向量的垂直长度
     * @param startPos 起始坐标
     * @param endPos 终点坐标
     * @param checkPos 检测坐标
     */
    public static GetVerticalLength(startPos: cc.Vec2, endPos: cc.Vec2, checkPos: cc.Vec2): number {
        let se = startPos.sub(endPos).magSqr()
        let p = (checkPos.x - startPos.x) * (endPos.x - startPos.x) + (checkPos.y - startPos.y) * (endPos.y - startPos.y)
        let r = p / se
        let newVec = new cc.Vec2(startPos.x + r * (endPos.x - startPos.x), startPos.y + r * (endPos.y - startPos.y))
        return newVec.sub(checkPos).mag()
    }
    /**
     * 判断是否相交
     * @param startPos 开始位置
     * @param endPos 结束位置
     * @param checkPos_1 检测线段起点
     * @param checkPos_2 检测线段终点
     */
    public static JudgeLineIntersert(startPos: cc.Vec2, endPos: cc.Vec2, checkPos_1: cc.Vec2, checkPos_2: cc.Vec2): boolean {
        var u, v, w, z
        u = (checkPos_1.x - startPos.x) * (endPos.y - startPos.y) - (endPos.x - startPos.x) * (checkPos_1.y - startPos.y);
        v = (checkPos_2.x - startPos.x) * (endPos.y - startPos.y) - (endPos.x - startPos.x) * (checkPos_2.y - startPos.y);
        w = (startPos.x - checkPos_1.x) * (checkPos_2.y - checkPos_1.y) - (checkPos_2.x - checkPos_1.x) * (startPos.y - checkPos_1.y);
        z = (endPos.x - checkPos_1.x) * (checkPos_2.y - checkPos_1.y) - (checkPos_2.x - checkPos_1.x) * (endPos.y - checkPos_1.y);
        return (u * v <= 0.00000001 && w * z <= 0.00000001);
    }
    /**
     * 加载时动画
     * @param parent 父物体
     */
    public static async LoadingTips(parent: cc.Node, cycName: string): Promise<void> {
        let eventBlock: cc.Node = PoolManager.GetObj('loadingWait')
        if (eventBlock == null)
            eventBlock = cc.instantiate(await this.LoadTip())
        eventBlock.active = true
        eventBlock.parent = parent
        eventBlock.x = 0
        eventBlock.y = 0
        eventBlock.zIndex = 999
        EventManager.EventPush({
            eventName: 'LoadFinish' + cycName, eventFunc: () => {
                GameMgr.CycleEventRemove('quanAni' + cycName)
                PoolManager.PutObj('loadingWait', eventBlock, () => {
                    eventBlock.active = false
                    eventBlock.parent = null
                    eventBlock.angle = 0
                })
            }, isOnce: true
        })
        this.LoadAni(eventBlock.getChildByName('quan'), cycName)
    }
    /**
     * 加载动画
     * @param quan 动画主体
     */
    private static LoadAni(quan: cc.Node, cycName: string): void {
        GameMgr.CycleEventPush({
            cycName: 'quanAni' + cycName, cycFunc: (dt: number) => {
                quan.angle -= dt * 50
            }
        })
    }
    /**
     * 加载动画预制体
     */
    private static async LoadTip(): Promise<any> {
        return new Promise<any>(async resolve => {
            // return resolve(await GameMgr.ResLoadByAB(ABName.commonUse, 'eventBlock', cc.Prefab))
        })
    }
    /**
     * 检测是否刘海
     * @param top 需要降低的对象
     */
    public static CheckBang(top: cc.Node): void {
        if (window.screen.width / window.screen.height >= 2) {
            top.getComponent(cc.Widget).left = 50
            top.x += 50
        }
        if (window.screen.height / window.screen.width >= 2.1) {
            top.getComponent(cc.Widget).top = 50
            top.y -= 50
        }
    }
    /**
     * 用图片展示数字
     * @param number_str 需要显示的数字
     * @param atlasName 图集名字
     * @param parent 父物体
     * @param sprite_arr 对象集合
     * @param changeUnit 是否需要变更单位
     * @param needPoint 是否需要标点符号
     * @param precentageSize 图片大小尺寸
     */
    public static ShowNumber(number_str: string, atlasName: string, parent: cc.Node, sprite_arr: cc.Sprite[], changeUnit = true, needPoint = true, precentageSize = 1): cc.Sprite[] {
        let code = 0, start = -1, newNum: string[] = []
        if (changeUnit) {
            for (let i = 0; i < number_str.length; i++) {
                code = number_str.charCodeAt(i)
                if (code > 57 || code < 48) {
                    if (i != 0) {
                        let tempNum = parseInt(number_str.substr(start + 1, i - start))
                        if (!isNaN(tempNum))
                            newNum = newNum.concat(ComonTools.ChangeUnit(tempNum, needPoint))
                        newNum.push(NumberToStr[number_str[i]])
                    }
                    start = i
                } else if (i == number_str.length - 1) {
                    let tempNum = parseInt(number_str.substr(start + 1, i - start))
                    if (!isNaN(tempNum))
                        newNum = newNum.concat(ComonTools.ChangeUnit(tempNum, needPoint))
                }
            }
            if (newNum.length < 1)
                newNum = newNum.concat(ComonTools.ChangeUnit(parseInt(number_str), needPoint))
        } else {
            for (let i = 0; i < number_str.length; i++) {
                code = number_str.charCodeAt(i)
                if (code > 57 || code < 48) {
                    newNum.push(NumberToStr[number_str[i]])
                } else {
                    newNum.push(number_str[i])
                }
            }
        }
        let number_spriteFrame: cc.SpriteFrame[] = []
        let atlas = GameMgr.GetNumberAtlasCache(atlasName)
        for (let i = 0; i < newNum.length; i++) {
            number_spriteFrame.push(atlas.getSpriteFrame(newNum[i]))
        }
        return this.CreateSprite(number_spriteFrame, parent, sprite_arr, precentageSize)
    }
    /**
     * 创建精灵
     * @param number_spriteFrame 精灵图片集合
     * @param parent 父物体
     * @param sprite_arr 对象集合
     * @param precentageSize 图片大小比例
     */
    private static CreateSprite(number_spriteFrame: cc.SpriteFrame[], parent: cc.Node, sprite_arr: cc.Sprite[], precentageSize: number): cc.Sprite[] {
        let temp_sprite = null
        if (sprite_arr.length < number_spriteFrame.length) {
            for (let i = sprite_arr.length; i < number_spriteFrame.length; i++) {
                temp_sprite = PoolManager.GetObj('number')
                if (temp_sprite == null) {
                    temp_sprite = GameMgr.GetRes<cc.Prefab>('numberPrefab')
                    sprite_arr[i] = cc.instantiate(temp_sprite).getComponent(cc.Sprite)
                } else {
                    sprite_arr[i] = temp_sprite.getComponent(cc.Sprite)
                }
            }
        } else if (sprite_arr.length > number_spriteFrame.length) {
            for (let i = sprite_arr.length - 1; i >= number_spriteFrame.length; i--) {
                PoolManager.PutObj('number', sprite_arr[i].node, () => {
                    sprite_arr[i].node.parent = null
                    sprite_arr[i].enabled = false
                })
                sprite_arr[i] = null
            }
            sprite_arr.length = number_spriteFrame.length
        }
        let front_arr = new Array<cc.Sprite>()
        let back_arr = new Array<cc.Sprite>()
        let middle_num = sprite_arr.length % 2 == 0 ? sprite_arr.length / 2 : Math.round(sprite_arr.length / 2) - 1
        for (let i = 0; i < sprite_arr.length; i++) {
            sprite_arr[i].spriteFrame = number_spriteFrame[i]
            sprite_arr[i].node.parent = parent
            sprite_arr[i].node.width = sprite_arr[i].spriteFrame.getOriginalSize().width * precentageSize
            sprite_arr[i].node.height = sprite_arr[i].spriteFrame.getOriginalSize().height * precentageSize
            sprite_arr[i].node.y = 0
            sprite_arr[i].enabled = true
            if (sprite_arr.length % 2 == 0) {
                if (i < middle_num) front_arr.push(sprite_arr[i])
                else back_arr.push(sprite_arr[i])
            } else {
                if (i < middle_num) front_arr.push(sprite_arr[i])
                else if (i > middle_num) back_arr.push(sprite_arr[i])
                else sprite_arr[i].node.x = 0
            }
        }
        for (let i = front_arr.length - 1; i >= 0; i--) {
            if (sprite_arr.length % 2 == 0) {
                if (i == front_arr.length - 1)
                    front_arr[i].node.x = -(front_arr[i].node.width + back_arr[0].node.width) / 4
                else
                    front_arr[i].node.x = front_arr[i + 1].node.x - (front_arr[i + 1].node.width / 2 + front_arr[i].node.width / 2)
            } else {
                if (i == front_arr.length - 1)
                    front_arr[i].node.x = -(sprite_arr[middle_num].node.width / 2 + front_arr[i].node.width / 2)
                else
                    front_arr[i].node.x = front_arr[i + 1].node.x - (front_arr[i + 1].node.width / 2 + front_arr[i].node.width / 2)
            }
        }
        for (let i = 0; i < back_arr.length; i++) {
            if (sprite_arr.length % 2 == 0) {
                if (i == 0)
                    back_arr[i].node.x = (front_arr[front_arr.length - 1].node.width + back_arr[i].node.width) / 4
                else
                    back_arr[i].node.x = back_arr[i - 1].node.x + (back_arr[i - 1].node.width / 2 + back_arr[i].node.width / 2)
            } else {
                if (i == 0)
                    back_arr[i].node.x = sprite_arr[middle_num].node.width / 2 + back_arr[i].node.width / 2
                else
                    back_arr[i].node.x = back_arr[i - 1].node.x + (back_arr[i - 1].node.width / 2 + back_arr[i].node.width / 2)
            }
        }
        return sprite_arr
    }
    /**
     * 改变数字单位
     * @param tempNum 数字
     * @param needPoint 是否需要分数
     */
    public static ChangeUnit(tempNum: number, needPoint: boolean, needChange = true): Array<string> {
        let str = '', str_arr: string[] = []
        if (tempNum >= 1000) {
            if (needPoint) {
                // str = (tempNum / 1000).toFixed(1) + 'k'
                str = this.TransformUnit(tempNum)
            } else {
                str = Math.floor(tempNum / 1000) + 'k'
            }
        } else {
            str = tempNum.toString()
        }
        if (!needChange) {//不需要转字符
            for (let i = 0; i < str.length; i++)
                str_arr.push(str[i])
            return str_arr
        }
        for (let i = 0; i < str.length; i++) {
            str_arr.push(str[i])
            if (str_arr[i] == '.')
                str_arr[i] = NumberToStr['.']
        }
        return str_arr
    }
    /**
     * 更换单位
     * @param tempNum 需要转单位得数字
     */
    private static TransformUnit(tempNum: number): string {
        let return_str = ''
        let temp_str = tempNum.toString()//转为字符
        let temp_unit = ['k', 'm', 'g', 'b', 't']//单位数组
        let unit_index = Math.floor((temp_str.length + 1) / 4) - 1//获取单位下标
        let integerLength = (temp_str.length - 1) % 3 + 1//获取整数长度
        if (integerLength == 1) {
            return_str = temp_str[0] + '.' + temp_str[1] + temp_str[2] + temp_unit[unit_index]
        } else if (integerLength == 2) {
            return_str = temp_str[0] + temp_str[1] + '.' + temp_str[2] + temp_unit[unit_index]
        } else {
            return_str = temp_str[0] + temp_str[1] + temp_str[2] + temp_unit[unit_index]
        }
        return return_str
    }
    /**
     * 更新倒计时显示
     * @param time 时间
     */
    public static UpdateCountDownLabel(time: number) {
        if (time < 0) {
            return '00:00'
        }
        let min = 0, second = 0, min_str = '00', second_str = '00'
        if (time >= 60) {
            min = Math.floor(time / 60)
            second = time % 60
            if (min >= 10)
                min_str = min + ''
            else
                min_str = '0' + min
        } else {
            second = time
        }
        if (second >= 10) second_str = second + ''
        else second_str = '0' + second
        return min_str + ':' + second_str
    }
    /**
     * 检查是否超越边界
     * @param dis 距离
     * @param arr 数组
     * @param right_level 最右等级上限
     * @param left_level 最左等级下线
     * @param maxLevel 最高等级
     * @param minLevel 最低等级
     */
    public static CheckLittlePanelMove(dis: number, arr: Array<cc.Node> | Array<cc.Node>, right_level: number, left_level: number, maxLevel: number, minLevel: number) {
        if (dis < 0) {
            if (right_level >= maxLevel) {
                let check_x = arr[ComonTools.GetNearIndex(true, arr)].x
                dis = check_x - dis > 228 ? 228 - check_x : dis
            }
        } else {
            if (left_level <= minLevel) {
                let check_x = arr[ComonTools.GetNearIndex(false, arr)].x
                dis = check_x + dis > -228 ? -228 - check_x : dis
            }
        }
        return dis
    }
    /**
     * 获取最靠边缘的对象下标
     * @param left_right 是否向左移（true：左移，false：右移）
     * @param arr 数组
     */
    public static GetNearIndex(left_right: boolean, arr: Array<cc.Node> | Array<cc.Node>) {
        let x = left_right ? 228 : -228, index = 0, min = 228
        for (let i = 0; i < arr.length; i++) {
            let temp_min = Math.abs(arr[i].x - x)
            if (temp_min < min) {
                min = temp_min
                index = i
            }
        }
        return index
    }
    /**
     * 回弹
     * @param arr 数组
     * @param callBack 回调
     */
    public static BackPos(arr: Array<cc.Node> | Array<cc.Node>, callBack: Function) {
        let backPos: number[] = new Array<number>()
        for (let i = 0; i < arr.length; i++) {//228
            if (arr[i].x >= -114 && arr[i].x <= 114) {
                backPos.push(0)
            } else if (arr[i].x >= -342 && arr[i].x < -114) {
                backPos.push(-228)
            } else if (arr[i].x < -342) {
                backPos.push(-456)
            } else if (arr[i].x > 114 && arr[i].x <= 342) {
                backPos.push(228)
            } else {
                backPos.push(456)
            }
        }
        for (let i = 0; i < arr.length; i++) {
            cc.tween(arr[i])
                .to(0.2, { x: backPos[i] })
                .call(() => {
                    if (i == 0) {
                        callBack()
                    }
                })
                .start()
        }
    }
}
