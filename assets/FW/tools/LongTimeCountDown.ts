import GameMgr from '../manager/GameMgr';
import GameData from '../data/GameData';
/**
 * 脚本功能：倒计时
 * 修改内容：
 * 修改时间：
 */
const { ccclass } = cc._decorator;

@ccclass
export default class LongTimeCountDown extends cc.Component {
    /**
     * 体力恢复倒计时事件添加
     */
    public PowerAddCountDown() {
        if (GameMgr.ContainsKeyInTimer('PowerAdd')) return
        let time = 600, countDown = 0
        GameMgr.Timer({
            cycName: 'PowerAdd', cycFunc: () => {
                countDown += 1
                if (countDown >= time) {
                    countDown = 0
                    let currentPower = GameData.GetSingleData('currentPower') as number
                    let maxPower = GameData.GetCommonData('maxPower')
                    if (currentPower < maxPower) {
                        GameData.SetSingleData('currentPower', currentPower + 1)
                    }
                } else {
                    // const str = ComonTools.UpdateCountDownLabel(time - countDown)
                    // EventManager.EventPlay('MainPanelPowerCountDown', str)
                    // EventManager.EventPlay('WinnerPanelCountDownShow', str)
                }
            }
        }, this, 1, cc.macro.REPEAT_FOREVER)
    }
    /**
     * 挑战倒计时
     * @param secondCallBack 每秒回调
     * @param endCallBack 结束回调
     */
    public ChallengeCountDown(secondCallBack: Function, endCallBack: Function) {
        if (GameMgr.ContainsKeyInTimer('challengeCountDownTime')) return
        let zero_hour = 24
        let zero_min = 60
        let zero_sec = 60
        let date = new Date()
        let date_hour = date.getHours()
        let date_min = date.getMinutes()
        let date_sec = date.getSeconds()
        let new_hour = zero_hour - date_hour, new_min = zero_min - date_min, new_sec = zero_sec - date_sec
        secondCallBack(new_hour, new_min, new_sec)
        GameMgr.Timer({
            cycName: 'challengeCountDownTime', cycFunc: () => {
                secondCallBack(new_hour, new_min, new_sec)
                if (new_sec == 0) {
                    new_sec = 59
                    new_min -= 1
                } else {
                    new_sec -= 1
                }
                if (new_min == 0) {
                    new_min = 59
                    new_hour -= 1
                }
                if (new_hour == 0) {
                    new_hour = 24
                    new_min = 59
                    new_hour = 59
                    GameMgr.UnTimer(this, 'challengeCountDownTime')
                    GameData.SetSingleData('challenge', 0)
                    endCallBack()
                }
            }
        }, this, 1, cc.macro.REPEAT_FOREVER)
    }
    public JumpCountDownAni(TimerName: string, callback: Function) {
        if (GameMgr.ContainsKeyInTimer(TimerName)) return
        GameMgr.Timer({
            cycName: TimerName, cycFunc: () => {
                callback()
            }
        }, this, 1, cc.macro.REPEAT_FOREVER)
    }
    /**
     * 插屏再次展示倒计时
     * @param callback 回调
     */
    public IntersADSShowCountDown(callback: Function) {
        GameMgr.Timer({
            cycName: 'IntersShow', cycFunc: () => {
                callback()
            }
        }, this, 15)
    }
}
