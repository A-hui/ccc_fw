/**资源信息 */
type AssetsInfo = {
    /**资源使用的时间 */
    useTime: number;
    /**资源本身 */
    asset: cc.Asset;
    /**资源url */
    url: string;
};

/**文件夹下面的文件信息 */
type DirInfo = {
    /**文件全局唯一id */
    uuid: string;
    /**文件相对路径，如：'Pool'文件夹下面就是，'Pool/Item' */
    path: string;
    /**构造函数 */
    ctor: Function;
};

export class GAssets {
    /**各个场景对应的图片资源列表 */
    private _assetsMap: Map<string, Map<string, AssetsInfo>> = new Map();

    /**场景资源释放的阈值，当前场景图片资源大小大于这个阈值的时候会自动释放使用时机最早的图片, Infinity表示默认不释放  */
    private _releaseThreshold = Infinity;

    /**所有资源存放的bundle的名字，单bundle模式下使用，一般为 resources */
    private _aloneBundle = '';

    // /**初始化一些默认参数 */
    // constructor() {
    //     //设置下载的最大并发连接数
    //     cc.assetManager.downloader.maxConcurrency = 1000;
    //     //用于设置每帧发起的最大请求数
    //     cc.assetManager.downloader.maxRequestsPerFrame = 1000;
    //     //失败重试次数
    //     cc.assetManager.downloader.maxRetryCount = 6;
    //     //失败重试的间隔时间
    //     cc.assetManager.downloader.retryInterval = 500;
    // }

    /**设置场景资源释放的阈值，单位M */
    setReleaseThreshold(max: number): void {
        this._releaseThreshold = max;
    }

    /**设置所有资源所在的bundle名字，开启单bundle模式，默认为 resources
     * 单bundle模式下，加载路径不需要带bundle名，直接写相对此bundle的相对路径就行
     */
    setAloneBundle(bundleName = 'resources') {
        this._aloneBundle = bundleName;
    }

    /**路径修正，如果开启单bundle模式，则给路径加上bundle名 */
    private pathFix(path: string): string {
        if (this._aloneBundle && !path.startsWith(this._aloneBundle)) {
            path = `${this._aloneBundle}/${path}`;
        }
        return path;
    }

    /**加载场景
     * @param scenePath 场景名，或者 ${bundle}/${path in bundle}
     * @param onLaunched 可选 加载完成后的回调
     */
    loadScene(scenePath: string, onLaunched?: (err: Error, scene: cc.Scene) => void): boolean {
        const load = (url: string): boolean => {
            //加载完后释放远程加载的资源
            const onLaunchedCallBack = (err: Error, scene: cc.Scene): void => {
                onLaunched && onLaunched(err, scene);
                if (!err) {
                    this.releaseAssets('Remote');
                }
            };
            return cc.director.loadScene(url, onLaunchedCallBack);
        };
        const { bundleName, path, name } = this.pathParse(scenePath);
        if (path === '') {
            //加载包内的场景
            return load(scenePath);
        } else {
            //加载bundle内的场景
            if (!cc.assetManager.getBundle(bundleName)) {
                console.warn('loadScene请先加载场景所在bundle');
                this.loadBundle(bundleName, (error: Error) => {
                    if (error) {
                        console.error(`加载对应的bundle失败:${bundleName}`);
                        onLaunched && onLaunched(new Error(`loadScene加载对应的bundle失败:${bundleName}`), null);
                    } else {
                        return this.loadScene(scenePath, onLaunched);
                    }
                });
            } else {
                const bundle = cc.assetManager.getBundle(bundleName);
                const info: { url?: string } = bundle.getSceneInfo(name);
                return load(info.url);
            }
        }
    }

    /**预加载场景
     * @param scenePath 场景路径，如果是在某一个bundle下面的场景，则要加上所在的bundle名，如：'home_core/home'
     */
    preloadScene(
        scenePath: string,
        onProgress?: (completedCount: number, totalCount: number, item) => void,
        onLoaded?: (error: Error) => void
    ): void {
        const { bundleName, path } = this.pathParse(scenePath);
        if (path === '') {
            //预加载包内的场景
            return cc.director.preloadScene(scenePath, onProgress, onLoaded);
        } else {
            //预加载bundle内的场景
            if (!cc.assetManager.getBundle(bundleName)) {
                console.warn('preloadScene请先加载场景所在bundle');
                this.loadBundle(bundleName, (error: Error) => {
                    if (error) {
                        console.error(`加载对应的bundle失败:${bundleName}`);
                        onLoaded && onLoaded(new Error(`preloadScene加载对应的bundle失败:${bundleName}`));
                    } else {
                        return this.preloadScene(scenePath, onProgress, onLoaded);
                    }
                });
            } else {
                const bundle = cc.assetManager.getBundle(bundleName);
                const info: { url?: string } = bundle.getSceneInfo(path);
                return cc.director.preloadScene(info.url, onProgress, onLoaded);
            }
        }
    }

    /**加载远程资源（头像等），异步版本，这个接口加载的资源将会在场景切换完成后释放，资源分组为：'Remote' */
    async loadRemoteAsync<T extends typeof cc.Asset>(
        url: string,
        type: T,
        context?: { isValid: boolean }
    ): Promise<InstanceType<T>> {
        return this.loadAsync(url, type, context, 'Remote');
    }

    /**加载远程资源（头像等），这个接口加载的资源将会在场景切换完成后释放，资源分组为：'Remote' */
    loadRemote<T extends typeof cc.Asset>(
        url: string,
        type: T,
        completeCallback: (error: Error, resource: InstanceType<T>) => void,
        context?: { isValid: boolean }
    ): void {
        this.load(url, type, completeCallback, context, 'Remote');
    }

    /**加载自定义bundle，可以加载一个或者多个bundle，异步版本 */
    async loadBundleAsync(bundleList: string | string[], isOrder = false): Promise<cc.AssetManager.Bundle[]> {
        return new Promise((resolve, reject) => {
            this.loadBundle(
                bundleList,
                (error: Error, bundleLoadList: cc.AssetManager.Bundle[]) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(bundleLoadList);
                    }
                },
                isOrder
            );
        });
    }

    /**加载自定义bundle，可以加载一个或者多个bundle
     * @param bundleList 要加载的 bundle 名字（远程路径）或数组
     * @param completeCallback 加载完成后的回调
     * @param isOrder 可选参数，是否一个一个下载，默认同时一起下载
     */
    loadBundle(
        bundleList: string | string[],
        completeCallback: (error: Error, bundleLoadList: cc.AssetManager.Bundle[]) => void,
        isOrder = false
    ): void {
        if (typeof bundleList === 'string') {
            bundleList = [bundleList];
        }
        const max = bundleList.length;
        const loadFailList: string[] = [];
        const bundleLoadList: cc.AssetManager.Bundle[] = [];
        const loadOne = (one: string, onFinish?: (isFinish: boolean) => void): void => {
            cc.assetManager.loadBundle(one, (err: Error, bundle: cc.AssetManager.Bundle) => {
                if (err) {
                    console.error('加载bundle错误:', one, err.message || err);
                    loadFailList.push(one);
                } else {
                    console.log('加载bundle成功', one, bundle);
                    bundleLoadList.push(bundle);
                }
                const isFinish = loadFailList.length + bundleLoadList.length === max;
                if (isFinish) {
                    //全部加载完成
                    if (loadFailList.length > 0) {
                        completeCallback(
                            new Error(`loadBundle未全部下载成功，失败列表:${loadFailList.toString()}`),
                            bundleLoadList
                        );
                    } else {
                        completeCallback(null, bundleLoadList);
                    }
                }
                onFinish && onFinish(isFinish);
            });
        };
        if (isOrder) {
            let i = 0;
            const loadOneOrder = (): void => {
                loadOne(bundleList[i], (isFinish: boolean) => {
                    if (!isFinish) {
                        i++;
                        loadOneOrder();
                    }
                });
            };
            loadOneOrder();
        } else {
            //一起下载
            for (let i = 0; i < max; i++) {
                const one = bundleList[i];
                loadOne(one);
            }
        }
    }

    /**路径解析，分离出bundle名和资源路径 */
    private pathParse(url: string): { bundleName: string; path: string; name: string } {
        url = this.pathFix(url);
        const arr = url.split('/');
        let bundleName = '';
        let path = '';
        let name = '';
        if (arr.length > 1) {
            bundleName = arr[0];
            path = url.slice(bundleName.length + 1);
            name = arr[arr.length - 1];
        } else {
            bundleName = url;
            path = '';
        }
        return { bundleName, path, name };
    }

    /**加载各种资源，异步版本
     * @param url bundle名字+相关bundle的路径，如在common这个bundle下面的prefab文件夹下面的Test预制，
     * url则为：'common/prefab/Test'，远程http加载的头像资源则为http地址
     * @param type 资源类型
     * @param context 当前脚本，可选，防止脚本销毁造成的泄露
     * @param group 资源组名，可选，默认为当前场景名
     */
    async loadAsync<T extends typeof cc.Asset>(
        url: string,
        type: T,
        context?: { isValid: boolean },
        group = cc.director.getScene().name
    ): Promise<InstanceType<T>> {
        return new Promise((resolve, reject) => {
            this.load(
                url,
                type,
                (error: Error, resource: InstanceType<T>) => {
                    if (!context || context.isValid) {
                        if (error) {
                            reject(error);
                        } else {
                            resolve(resource);
                        }
                    } else {
                        reject('脚本失效!');
                    }
                },
                undefined,
                group
            );
        });
    }

    /**加载各种资源
     * @param url bundle名字+相关bundle的路径，如在common这个bundle下面的prefab文件夹下面的Test预制，
     * url则为：'common/prefab/Test'，远程http加载的头像资源则为http地址
     * @param type 资源类型
     * @param completeCallback 可选， 加载完成后的回调，脚本失效的时候将不调用此回调
     * @param context 当前脚本，可选，防止脚本销毁造成的泄露
     * @param group 资源组名，可选，默认为当前场景名
     */
    load<T extends typeof cc.Asset>(
        url: string,
        type: T,
        completeCallback: (error: Error, resource: InstanceType<T>) => void,
        context?: { isValid: boolean },
        group = cc.director.getScene().name
    ): void {
        if (!context || context.isValid) {
            /**结果处理 */
            const complete = (error: Error, resource: InstanceType<T>): void => {
                if (error) {
                    completeCallback(error, null);
                } else {
                    if (!context || context.isValid) {
                        //需要记录的类型
                        const recordArr = ['cc.Prefab', 'cc.SpriteFrame', 'sp.SkeletonData', 'cc.Texture2D'];
                        if (recordArr.indexOf(resource['__classname__']) > -1) {
                            this.addAssets(url, resource, group);
                        }
                        completeCallback(null, resource);
                    } else {
                        console.error('context组件已经失效了');
                        completeCallback(new Error('context组件已经失效了'), null);
                    }
                }
            };
            //判断是否是远程资源
            if (url.startsWith('http')) {
                //远程加载的资源
                if ((type as unknown) === cc.SpriteFrame) {
                    const img = new Image();
                    img.src = url;
                    img.crossOrigin = 'anonymous';
                    img.onload = () => {
                        img['_onloaded_'] = true;
                        const tex = new cc.Texture2D();
                        tex.initWithElement(img);
                        const sf = new cc.SpriteFrame(tex);
                        complete(null, sf as InstanceType<T>);
                    };
                    img.onerror = (ev: string | Event) => {
                        if (!img['_onloaded_']) {
                            console.error('加载远程资源失败', url, ev);
                            complete(new Error('加载远程资源失败'), null);
                        }
                    };
                } else {
                    cc.assetManager.loadRemote(url, (error: Error, resource) => {
                        if ((type as unknown) === cc.SpriteFrame) {
                            resource = new cc.SpriteFrame(resource as cc.Texture2D);
                        }
                        complete(error, resource as InstanceType<T>);
                    });
                }
            } else {
                const { bundleName, path } = this.pathParse(url);
                const bundle = cc.assetManager.getBundle(bundleName);
                if (bundle) {
                    let asset = bundle.get(path, type);
                    if (asset && asset.isValid && asset.refCount >= 0) {
                        complete(null, asset as InstanceType<T>);
                    } else {
                        //加载资源
                        bundle.load(path, type, (error: Error, resource: cc.Asset) => {
                            if (error) {
                                completeCallback(error, null);
                            } else {
                                if (resource.isValid && resource.refCount >= 0) {
                                    complete(error, resource as InstanceType<T>);
                                } else {
                                    //被释放了，重新加载
                                    this.load(url, type, completeCallback, context, group);
                                }
                            }
                        });
                    }
                } else {
                    console.warn('load请先下载资源所在bundle', url);
                    this.loadBundle(bundleName, (error: Error) => {
                        if (error) {
                            console.error(`加载对应的bundle失败:${bundleName}`);
                            completeCallback(new Error(`loadBundle加载对应的bundle失败:${bundleName}`), null);
                        } else {
                            return this.load(url, type, completeCallback, context, group);
                        }
                    });
                }
            }
        } else {
            console.error('无效的组件');
            completeCallback(new Error('无效的组件'), null);
        }
    }

    /**获取一个bundle所有资源相对路径 */
    private getBundleAllAssetsPath(bundle: cc.AssetManager.Bundle): string[] {
        return Object.keys(bundle['_config']['paths']['_map']);
    }

    /**获取一个bundle所有场景路径 */
    private getBundleAllScenePath(bundle: cc.AssetManager.Bundle): string[] {
        return Object.keys(bundle['_config']['scenes']['_map']);
    }

    /**获取某个文件夹下面所有文件信息 */
    getDirWithPath(url: string): DirInfo[] {
        const { bundleName, path } = this.pathParse(url);
        const bundle = cc.assetManager.getBundle(bundleName);
        if (bundle) {
            return bundle.getDirWithPath(url) as DirInfo[];
        } else {
            console.error('没找到对应bundle!', url);
        }
    }

    /**预加载单个资源
     * @param url 相对bundle的路径，如在common这个bundle下面的prefab文件夹下面的Test预制，
     * url则为：'common/prefab/Test',
     * @param completeCallback 可选 预加载完成后的回调
     */
    preload(url: string, onComplete?: (error: Error, items: cc.AssetManager.RequestItem[]) => void): void {
        //查找资源对应的bundle
        const { bundleName, path } = this.pathParse(url);
        const bundle = cc.assetManager.getBundle(bundleName);
        if (bundle) {
            bundle.preload(path, (error: Error, items: cc.AssetManager.RequestItem[]) => {
                onComplete && onComplete(error, items);
            });
        } else {
            console.warn('preload请先下载资源所在bundle', url);
            this.loadBundle(bundleName, (error: Error) => {
                if (error) {
                    console.error(`加载对应的bundle失败:${bundleName}`);
                    onComplete && onComplete(new Error(`preload加载对应的bundle失败:${bundleName}`), null);
                } else {
                    return this.preload(url, onComplete);
                }
            });
        }
    }

    /**预加载整个文件夹的资源
     *  @param folderPath 文件夹路径，如预加载common这个bundle下面的prefab文件夹下面的所有预制，则 folderPath为: 'common/prefab',
     * 预加载common这个bundle下面的所有资源，则 folderPath为: 'common',
     * @param onComplete 可选 预加载完成后的回调
     */
    preloadDir(folderPath: string, onComplete?: (error: Error, items: cc.AssetManager.RequestItem[][]) => void): void {
        //查找该文件夹下面的所有资源
        const { bundleName, path } = this.pathParse(folderPath);
        const bundle = cc.assetManager.getBundle(bundleName);
        //加载失败列表
        const failList: string[] = [];
        //加载成功列表
        const successList: cc.AssetManager.RequestItem[][] = [];
        if (!bundle) {
            console.warn('preloadDir请先下载资源所在bundle', folderPath);
            this.loadBundle(bundleName, (error: Error) => {
                if (error) {
                    console.error(`加载对应的bundle失败:${bundleName}`);
                    onComplete && onComplete(new Error(`preloadDir加载对应的bundle失败:${bundleName}`), null);
                } else {
                    return this.preloadDir(folderPath, onComplete);
                }
            });
        } else {
            let assetsCount = 0;
            for (const one of this.getBundleAllAssetsPath(bundle)) {
                if (one.startsWith(path)) {
                    assetsCount++;
                }
            }
            let sceneCount = 0;
            for (const one of this.getBundleAllScenePath(bundle)) {
                if (one.indexOf(path) > -1) {
                    sceneCount++;
                }
            }
            console.warn('资源总数', assetsCount);
            console.warn('场景总数', sceneCount);
            //判断加载是否结束
            const isFinish = (): void => {
                if (failList.length + successList.length === assetsCount + sceneCount) {
                    console.warn('加载所有资源完成');
                    //全部加载完成
                    if (failList.length > 0) {
                        onComplete(new Error(`preloadDir未全部下载成功，失败列表:${failList.toString()}`), successList);
                    } else {
                        onComplete(null, successList);
                    }
                }
            };
            //预加载所有资源
            for (const one of this.getBundleAllAssetsPath(bundle)) {
                if (one.startsWith(path)) {
                    this.preload(`${bundleName}/${one}`, (error: Error, items: cc.AssetManager.RequestItem[]) => {
                        if (error) {
                            console.error('加载资源失败', `${bundleName}/${one}`);
                            failList.push(`${bundleName}/${one}`);
                        } else {
                            successList.push(items);
                        }
                        isFinish();
                    });
                }
            }

            //预加载所有场景
            for (const one of this.getBundleAllScenePath(bundle)) {
                if (one.indexOf(path) > -1) {
                    const loadList: cc.AssetManager.RequestItem[] = [];
                    this.preloadScene(
                        `${bundleName}/${one}`,
                        (completedCount: number, totalCount: number, item) => {
                            //console.log('preloadScene', completedCount, totalCount, item);
                            loadList.push(item);
                        },
                        (error: Error) => {
                            if (error) {
                                console.error('加载场景失败', `${bundleName}/${one}`);
                                failList.push(`${bundleName}/${one}`);
                            } else {
                                successList.push(loadList);
                            }
                            isFinish();
                        }
                    );
                }
            }
        }
    }

    /**添加资源管理 */
    private addAssets(url: string, asset: cc.Asset, group = cc.director.getScene().name): void {
        if (!this._assetsMap.has(group)) {
            this._assetsMap.set(group, new Map());
        }
        const map = this._assetsMap.get(group);
        const useTime = Date.now();
        const AssetsInfo: AssetsInfo = { useTime, asset, url };
        map.set(url, AssetsInfo);
        //查看当前缓存是否超出限制
        if (this._releaseThreshold !== Infinity && this.countPng() > this._releaseThreshold) {
            //超过最大限制，释放使用时机最早的资源
            const map = this._assetsMap.get(group);
            if (map) {
                const assetsArr: AssetsInfo[] = [];
                map.forEach((v) => assetsArr.push(v));
                assetsArr.sort((a, b) => {
                    return a.useTime - b.useTime;
                });
                while (this.countPng() > this._releaseThreshold) {
                    const asset = assetsArr.shift();
                    asset.asset.decRef();
                    //移除记录
                    map.delete(asset.url);
                }
            }
        }
    }

    /**释放某一组资源 */
    private releaseAssets(group: string): void {
        const map = this._assetsMap.get(group);
        if (map) {
            map.forEach((v, k) => {
                const asset = v.asset;
                //如果当前场景没有引用这个资源，就释放
                const nowMap = this._assetsMap.get(cc.director.getScene().name);
                if (!nowMap || !nowMap.get(k)) {
                    asset.decRef();
                } else {
                    console.log('本场景已经有引用，不释放', k);
                }
            });
            this._assetsMap.delete(group);
        }
    }

    /**计算当前图片资源大小 */
    private countPng(): number {
        const rawCacheData = cc.assetManager.assets['_map'];
        let allPngSize = 0;
        for (const key of Object.keys(rawCacheData)) {
            const value = rawCacheData[key];
            const __classname__ = value.__classname__;
            //如果是 cc.Texture2D 图片资源，就计算其大小
            if (__classname__ === 'cc.Texture2D') {
                const textureSize = value.width * value.height * ((value._native === '.jpg' ? 3 : 4) / 1024 / 1024);
                allPngSize += textureSize;
            }
        }
        return allPngSize;
    }
}

/**暴露全局提示 */
declare global {
    /**
     * 资源加载，释放，管理模块
     */
    const GAssets: GAssets;
}
window['GAssets'] = new GAssets();
