export default class GNumber {

    /**
     * 随机生成[min, max) 范围内的整数
     * @param min 最小值(包括)
     * @param max 最大值(不包括)
     */
    public static getRandomIntNum(min: number, max: number){
        return Math.floor(Math.random() * (max - min)) + min
    }

    /**
     * 随机生成 [min, max) 范围内的浮点数
     * @param min 最小值(包括)
     * @param max 最大值(不包括)
     * @returns 
     */
    public static getRandomFloatNum(min: number, max:number): number{
        return Math.random() * (max - min) + min
    }

    /**
     * 角度标准化， 把角度转换成0到360度
     * @param angle 角度
     * @returns 
     */
    public static angleToStandard(angle: number){
        let standerAngle = 0
        angle < 0 ? standerAngle = 360 - Math.abs(angle % 360) : standerAngle = angle % 360
        return standerAngle
    }
} 