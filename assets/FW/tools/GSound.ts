/**全局背景音乐Bgm，音效Effect播放控制模块  
 *所有Bgm和Effect都放在resources文件夹下面动态加载，推荐的默认文件夹结构为：  
 *--resources  （resources下有个文件夹Audio，用来放所有音乐文件）  
  ----Audio  （Audio下有2个文件夹Bgm，Effect）    
  ------Bgm  （Bgm文件夹下用来放所有Bgm，第一个音乐为默认Bgm）    
  ------Effect  （Effect文件夹用来放所有Effect）    
 * 如果不为上面的默认文件结构，可以在 init函数 里面设置你需要的默认参数  
   依赖模块：GAssets
 */
/**Bgm名字->路径的映射表 */
let BgmName2Path = {
	game_Game: 'game/Bgm/Game',
};

/**Bgm可选名字 */
type BgmName = keyof typeof BgmName2Path;

/**Effect名字->路径的映射表 */
let EffectName2Path = {
	resources_click: 'resources/Audio/Effect/click',
};

/**Effect可选名字 */
type EffectName = keyof typeof EffectName2Path;

/**Effect参数类型 */
type EffectPram = {
	/**是否循环播放 */
	isLoop: boolean;
	/**当前播放的音效id */
	audioID: number;
};

export class GAudio {
	/**==========  Bgm ==========*/
	/**默认Bgm的音量大小  1 */
	private _BgmVolume = 1;
	/**默认Bgm是否循环  true */
	private _BgmLoop = true;
	/**是否允许多个Bgm同时播放 false */
	private _BgmCanMultiple = false;

	/**==========  Effect ==========*/
	/**默认Effect的音量大小  1 */
	private _EffectVolume = 1;
	/**默认Effect是否循环  false */
	private _EffectLoop = false;
	/**是否允许多个Effect同时播放 true */
	private _EffectCanMultiple = true;

	/**保存所有Audio信息的map */
	private _allAudioInfo: Map<string, cc.AudioClip> = new Map();
	/**保存正在播放的Bgm的map */
	private _playingBgm: Map<string, number | symbol> = new Map();
	/**保存正在播放的Effect的map */
	private _playingEffect: Map<string, EffectPram | symbol> = new Map();

	/**是否为静音模式，静音模式下不会播放所有的Bgm和Effect */
	private _isMute = false;
	/**是否为Bgm静音模式，Bgm静音模式下不会播放Bgm */
	private _isBgmMute = false;
	/**是否为Effect静音模式，Effect静音模式下不会播放Effect */
	private _isEffectMute = false;

	/**当前正在播放的Bgm的名字，用来重新播放Bgm */
	private _playingBgmName: BgmName;

	/**音乐音效是否播放了 */
	private _isPlayed = Symbol();

	/**初始化设置Bgm和Effect的默认参数，一般都用默认的默认参数，
	 * - ***可不调用此函数***
	 * @param optional 属性参数说明如下：
	 * @param optional.AudioPath 保存所有Bgm的根目录，默认为"Audio"
	 * @param optional.BgmPath 保存所有Bgm的根目录，默认为"Bgm"
	 * @param optional.BgmVolume 默认Bgm的音量大小，默认为1
	 * @param optional.BgmLoop 默认Bgm是否循环，默认为true
	 * @param optional.BgmDefault 默认播放的Bgm的名字，如: "Bgm1"，默认为${_BgmPath}目录下的第一个文件
	 * @param optional.BgmCanMultiple 是否允许多个Bgm同时播放 默认为false
	 * @param optional.EffectPath 保存所有Effect的根目录，默认为"Effect"
	 * @param optional.EffectVolume 默认Effect的音量大小，默认为1
	 * @param optional.EffectLoop 默认Effect是否循环，默认为false
	 * @param optional.EffectCanMultiple 是否允许多个Effect同时播放 默认为true
	 * @param optional.isMute 默认是否为静音模式，默认为false
	 */
	init(
		optional: {
			AudioPath?: string;
			BgmPath?: string;
			BgmVolume?: number;
			BgmLoop?: boolean;
			BgmDefault?: BgmName;
			BgmCanMultiple?: boolean;
			EffectPath?: string;
			EffectVolume?: number;
			EffectLoop?: boolean;
			EffectCanMultiple?: boolean;
			isMute?: boolean;
		} = {}
	) {
		//设置了默认参数的替换默认的默认参数
		for (let key of Object.keys(optional)) {
			if (typeof this['_' + key] != 'undefined') {
				this['_' + key] = optional[key];
			}
		}
	}

	/**加载一个Audio（Bgm或者Effect）
	 * @param path Audio的相对路径
	 */
	private _loadAudio(path: string) {
		return new Promise((resolve, reject) => {
			GAssets.load(path, cc.AudioClip, (err, clip) => {
				if (err) {
					console.error(err.message || err);
					reject(err);
					return;
				}
				console.warn('预加载成功!', clip);
				if (!this._allAudioInfo.has(path)) {
					//记录当前的Audio
					this._allAudioInfo.set(path, clip);
				}
				resolve(clip);
			});
		});
	}

	/**预加载一个或多个Bgm
	 * @param name Bgm的名字或者名字数组
	 * @example
	 * GAudio.preloadBgm("Bgm1");
	 */
	preloadBgm(name: BgmName | BgmName[]) {
		if (Array.isArray(name)) {
			for (let one of name) {
				this._loadAudio(BgmName2Path[name as any]);
			}
		} else {
			this._loadAudio(BgmName2Path[name as any]);
		}
	}

	/**预加载所有Bgm
	 * @example
	 * GAudio.preloadAllBgm();
	 */
	preloadAllBgm() {
		this.preloadBgm(Object.keys(BgmName2Path) as BgmName[]);
	}

	/**预加载一个或多个Effect
	 * @param name Effect的名字或者名字数组
	 * @example
	 * GAudio.preloadEffect("effect1");
	 */
	preloadEffect(name: EffectName | EffectName[]) {
		if (Array.isArray(name)) {
			for (let one of name) {
				this._loadAudio(EffectName2Path[name as any]);
			}
		} else {
			this._loadAudio(EffectName2Path[name as any]);
		}
	}

	/**预加载所有Effect
	 * @example
	 * GAudio.preloadAllEffect();
	 */
	preloadAllEffect() {
		this.preloadEffect(Object.keys(EffectName2Path) as EffectName[]);
	}

	/**预加载所有Audio（Bgm和Effect）
	 * @example
	 * GAudio.preloadAll();
	 */
	preloadAll() {
		this.preloadAllBgm();
		this.preloadAllEffect();
	}

	/**播放Bgm
	 * @param name Bgm名字
	 * @param isLoop Bgm是否循环
	 * @param volume Bgm音量
	 */
	private _playBgm(name: BgmName, isLoop: boolean, volume: number) {
		let clip = this._allAudioInfo.get(BgmName2Path[name]);
		if (!this._BgmCanMultiple) {
			this.stopBgm(undefined, true);
		}
		//记录当前bgm已经开始播放
		this._playingBgm.set(BgmName2Path[name], this._isPlayed);
		this._playingBgmName = name;
		if (clip) {
			this._playBgmAndSet(clip, isLoop, volume, BgmName2Path[name]);
		} else {
			//还没加载过，开始加载并播放
			console.warn('还没加载过，开始加载并播放');
			let load = this._loadAudio(BgmName2Path[name]);
			load.then((clip: cc.AudioClip) => {
				this._playBgmAndSet(clip, isLoop, volume, BgmName2Path[name]);
			});
		}
	}

	/**播放Bgm并设置播放信息
	 * @param clip Bgm播放的AudioClip
	 * @param isLoop Bgm是否循环
	 * @param volume Bgm音量
	 * @param path Bgm路径
	 */
	private _playBgmAndSet(clip: cc.AudioClip, isLoop: boolean, volume: number, path: string) {
		if (this._playingBgm.get(path) === this._isPlayed) {
			let audioID = cc.audioEngine.play(clip, isLoop, volume);
			this._playingBgm.set(path, audioID);
			cc.audioEngine.setFinishCallback(audioID, () => {
				//播放完清理map
				this._playingBgm.delete(path);
			});
		}
	}

	/**播放背景音乐   所有默认行为都可以在init里面设置
	 * @param name Bgm名字，
	 * @param isLoop Bgm是否循环播放，默认为true
	 * @param volume Bgm的音量，默认为1
	 * @example
	 * GAudio.playBgm('resources_bgm');
	 */
	playBgm(name: BgmName, isLoop = this._BgmLoop, volume = this._BgmVolume) {
		this._playingBgmName = name;
		if (this._isMute || this._isBgmMute) return;
		this._playBgm(name, isLoop, volume);
	}

	/**停止正在播放的背景音乐
	 * @param name Bgm的名字或者名字数组，可以为空，为空时表示停止所有Bgm
	 * @param isAuto 是否是内部逻辑导致的自动停止，默认为false
	 * @example
	 * GAudio.stopBgm();
	 */
	stopBgm(name?: BgmName | BgmName[], isAuto = false) {
		if (Array.isArray(name)) {
			for (let one of name) {
				this._stopBgm(one);
			}
		} else {
			if (typeof name == 'undefined') {
				//停止所有Bgm
				this._playingBgm.forEach((audioID, path) => {
					cc.audioEngine.stop(audioID as number);
					if (audioID !== this._isPlayed || !isAuto) {
						this._playingBgm.delete(path);
					}
				});
			} else {
				this._stopBgm(name);
			}
		}
	}

	/**停止正在播放的背景音乐
	 * @param name Bgm的名字
	 */
	private _stopBgm(name: BgmName) {
		let path = BgmName2Path[name];
		let audioID = this._playingBgm.get(path);
		cc.audioEngine.stop(audioID as number);
		this._playingBgm.delete(path);
	}

	/**暂停正在播放的背景音乐
	 * @param name Bgm的名字或者名字数组，可以为空，为空时表示暂停所有Bgm
	 * @example
	 * GAudio.pauseBgm();
	 */
	pauseBgm(name?: BgmName | BgmName[]) {
		if (Array.isArray(name)) {
			for (let one of name) {
				this._pauseBgm(one);
			}
		} else {
			if (typeof name == 'undefined') {
				//暂停所有Bgm
				this._playingBgm.forEach((audioID, path) => {
					cc.audioEngine.pause(audioID as number);
				});
			} else {
				this._pauseBgm(name);
			}
		}
	}

	/**暂停正在播放的背景音乐
	 * @param name Bgm的名字
	 */
	private _pauseBgm(name: BgmName) {
		let path = BgmName2Path[name];
		let audioID = this._playingBgm.get(path);
		cc.audioEngine.pause(audioID as number);
	}

	/**恢复背景音乐
	 * @param name Bgm的名字或者名字数组，可以为空，为空时表示恢复所有Bgm
	 * @example
	 * GAudio.resumeBgm();
	 */
	resumeBgm(name?: BgmName | BgmName[]) {
		if (this._isMute || this._isBgmMute) return;
		if (Array.isArray(name)) {
			for (let one of name) {
				this._resumeBgm(one);
			}
		} else {
			if (typeof name == 'undefined') {
				//恢复所有Bgm
				this._playingBgm.forEach((audioID, path) => {
					cc.audioEngine.resume(audioID as number);
				});
			} else {
				this._resumeBgm(name);
			}
		}
	}

	/**恢复背景音乐
	 * @param name Bgm的名字
	 */
	private _resumeBgm(name: BgmName) {
		let path = BgmName2Path[name];
		let audioID = this._playingBgm.get(path);
		cc.audioEngine.resume(audioID as number);
	}

	/**播放音效   所有默认行为都可以在init里面设置
	 * @param name Effect名字
	 * @param isLoop Effect是否循环播放，默认为false
	 * @param volume Effect的音量，默认为1
	 * @example
	 * GAudio.playEffect("effect1");
	 */
	playEffect(name: EffectName, isLoop = this._EffectLoop, volume = this._EffectVolume) {
		if (this._isMute || this._isEffectMute) return;
		let path = EffectName2Path[name];
		let clip = this._allAudioInfo.get(path);
		//查看是否有在播放的音效
		let _playingEffect = this._playingEffect.get(path) as EffectPram;
		if (_playingEffect && _playingEffect.isLoop && isLoop) {
			console.error('有正在循环播放的音效', name, path, _playingEffect);
		} else {
			if (clip) {
				this._playEffectAndSet(clip, isLoop, volume, path);
			} else {
				//还没加载过，开始加载并播放
				console.warn('Effect还没加载过，开始加载并播放');
				let load = this._loadAudio(path);
				load.then((clip: cc.AudioClip) => {
					this._playEffectAndSet(clip, isLoop, volume, path);
				});
			}
		}
	}

	/**播放Effect并设置播放信息
	 * @param clip Effect播放的AudioClip
	 * @param isLoop Effect是否循环
	 * @param volume Effect音量
	 * @param path Effect路径
	 */
	private _playEffectAndSet(clip: cc.AudioClip, isLoop: boolean, volume: number, path: string) {
		if (!this._EffectCanMultiple) {
			this.stopEffect();
		}
		let audioID = cc.audioEngine.play(clip, isLoop, volume);
		this._playingEffect.set(path, { isLoop, audioID });
		cc.audioEngine.setFinishCallback(audioID, () => {
			//播放完清理map
			this._playingEffect.delete(path);
		});
	}

	/**停止正在播放的音效
	 * @param name Effect的名字或者名字数组，可以为空，为空时表示停止所有Effect
	 * @example
	 * GAudio.stopEffect();
	 */
	stopEffect(name?: EffectName | EffectName[]) {
		if (Array.isArray(name)) {
			for (let one of name) {
				this._stopEffect(one);
			}
		} else {
			if (typeof name == 'undefined') {
				//停止所有Effect
				this._playingEffect.forEach((v, k) => {
					this._stopEffect(null, k);
				});
			} else {
				this._stopEffect(name);
			}
		}
	}

	/**停止正在播放的音效
	 * @param name Effect的名字
	 * @param path Effect的路径
	 */
	private _stopEffect(name?: EffectName, path?: string) {
		path = path || EffectName2Path[name];
		let playInfo = this._playingEffect.get(path) as EffectPram;
		if (playInfo) {
			cc.audioEngine.stop(playInfo.audioID);
			this._playingEffect.delete(path);
		} else {
			console.error('停止了不存在的音效', name);
		}
	}

	/**暂停正在播放的音效
	 * @param name Effect的名字或者名字数组，可以为空，为空时表示暂停所有Effect
	 * @example
	 * GAudio.pauseEffect();
	 */
	pauseEffect(name?: EffectName | EffectName[]) {
		if (Array.isArray(name)) {
			for (let one of name) {
				this._pauseEffect(one);
			}
		} else {
			if (typeof name == 'undefined') {
				//暂停所有Effect
				this._playingEffect.forEach((v, k) => {
					this._pauseEffect(null, k);
				});
			} else {
				this._pauseEffect(name);
			}
		}
	}

	/**暂停正在播放的音效
	 * @param name Effect的名字
	 * @param path Effect的路径
	 */
	private _pauseEffect(name: EffectName, path?: string) {
		path = path || EffectName2Path[name];
		let playInfo = this._playingEffect.get(path) as EffectPram;
		if (playInfo) {
			cc.audioEngine.pause(playInfo.audioID);
		} else {
			console.error('暂停了不存在的音效', name);
		}
	}

	/**恢复音效
	 * @param name Effect的名字或者名字数组，可以为空，为空时表示恢复所有Effect
	 * @example
	 * GAudio.resumeEffect();
	 */
	resumeEffect(name?: EffectName | EffectName[]) {
		if (this._isMute || this._isEffectMute) return;
		if (Array.isArray(name)) {
			for (let one of name) {
				this._resumeEffect(one);
			}
		} else {
			if (typeof name == 'undefined') {
				//恢复所有Effect
				this._playingEffect.forEach((v, k) => {
					this._resumeEffect(null, k);
				});
			} else {
				this._resumeEffect(name);
			}
		}
	}

	/**恢复音效
	 * @param name Effect的名字
	 * @param path Effect的路径
	 */
	private _resumeEffect(name: EffectName, path?: string) {
		path = path || EffectName2Path[name];
		let playInfo = this._playingEffect.get(path) as EffectPram;
		if (playInfo) {
			cc.audioEngine.resume(playInfo.audioID);
		} else {
			console.error('恢复了不存在的音效', name);
		}
	}

	/**停止所有Audio（Bgm和Effect）
	 * @example
	 * GAudio.stopAll();
	 */
	stopAll() {
		this.stopBgm();
		this.stopEffect();
	}

	/**暂停所有Audio（Bgm和Effect）
	 * @example
	 * GAudio.pauseAll();
	 */
	pauseAll() {
		this.pauseBgm();
		this.pauseEffect();
	}

	/**恢复所有Audio（Bgm和Effect）
	 * @example
	 * GAudio.resumeAll();
	 */
	resumeAll() {
		if (this._isMute) return;
		if (!this._isBgmMute) {
			this.resumeBgm();
		}
		if (!this._isEffectMute) {
			this.resumeEffect();
		}
	}

	/**设置声音模式
	 * @param isMute 是否为静音模式，
	 * true表示设置为静音，会暂停所有背景音乐和音效，并且不能播放和恢复所有Audio
	 * false表示取消静音，会恢复所有的背景音乐和音效，并且能播放和恢复其他Audio
	 * @example
	 * GAudio.setAudioMode(true);
	 */
	setAudioMode(isMute: boolean) {
		this._isMute = isMute;
		if (this._isMute) {
			this.pauseAll();
		} else {
			this.resumeAll();
		}
	}

	/**切换当前的声音模式，返回当前是否为静音模式
	 * @example
	 * GAudio.audioModeChange();
	 */
	audioModeChange() {
		let isMute = !this._isMute;
		this.setAudioMode(isMute);
		return isMute;
	}

	/**获取当前的是否为静音状态
	 * @example
	 * GAudio.getIsMute();
	 */
	getIsMute() {
		return this._isMute;
	}

	/**设置Bgm模式
	 * @param isMute 是否为静音模式，
	 * true表示设置为静音，会停止当前播放的Bgm
	 * false表示取消静音，会重新开始播放Bgm
	 * @example
	 * GAudio.setBgmMode(true);
	 */
	setBgmMode(isMute: boolean) {
		this._isBgmMute = isMute;
		if (this._isBgmMute) {
			this.stopBgm();
		} else {
			this.playBgm(this._playingBgmName);
		}
	}

	/**切换当前的Bgm模式，返回当前是否为静音模式
	 * @example
	 * GAudio.bgmModeChange();
	 */
	bgmModeChange() {
		let isBgmMute = !this._isBgmMute;
		this.setBgmMode(isBgmMute);
		return isBgmMute;
	}

	/**获取当前的Bgm模式
	 * @example
	 * GAudio.getIsBgmMute();
	 */
	getIsBgmMute() {
		return this._isBgmMute;
	}

	/**设置Effect模式
	 * @param isMute 是否为静音模式，
	 * true表示设置为静音，会停止当前所有播放的Effect
	 * false表示取消静音，
	 * @example
	 * GAudio.setEffectMode(true);
	 */
	setEffectMode(isMute: boolean) {
		this._isEffectMute = isMute;
		if (this._isEffectMute) {
			this.stopEffect();
		} else {
			console.warn('音效开启了');
		}
	}

	/**切换当前的Effect模式，返回当前是否为静音模式
	 * @example
	 * GAudio.effectModeChange();
	 */
	effectModeChange() {
		let isEffectMute = !this._isEffectMute;
		this.setEffectMode(isEffectMute);
		return isEffectMute;
	}

	/**获取当前的Effect模式
	 * @example
	 * GAudio.getIsEffectMute();
	 */
	getIsEffectMute() {
		return this._isEffectMute;
	}
}
/**暴露全局提示 */
declare global {
	/**全局背景音乐Bgm，音效Effect播放控制模块
	 *所有Bgm和Effect都放在resources文件夹下面动态加载，推荐的默认文件夹结构为：
	 *--resources  （resources下有个文件夹Audio，用来放所有音乐文件）
	 *----Audio  （Audio下有2个文件夹Bgm，Effect）
	 *------Bgm  （Bgm文件夹下用来放所有Bgm，第一个音乐为默认Bgm）
	 *------Effect  （Effect文件夹用来放所有Effect）
	 *如果不为上面的默认文件结构，可以在 init函数 里面设置你需要的默认参数
	 *依赖模块：GAssets
	 */
	const GAudio: GAudio;
}
window['GAudio'] = new GAudio();
