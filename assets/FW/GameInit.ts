import { ADSName, ABName, PrefabName, AtlasName } from "./data/CommonData";
import GameData from './data/GameData';
import GameMgr from './manager/GameMgr';
import { Scene_Data } from "./data/SceneData";
import UIManager from "./manager/UIManager";
import LongTimeCountDown from './tools/LongTimeCountDown';
import ComonTools from "./tools/ComonTools";
import ScrollList from "./tools/ScrollList";


const { ccclass } = cc._decorator;

@ccclass
export default class GameInit extends cc.Component {
    private judgeFinishLoad: boolean
    private loadSpeed: number
    private ani_stop: boolean
    public _GameInit(bar: cc.ProgressBar) {
        this.ani_stop = false
        this.judgeFinishLoad = false
        this.loadSpeed = 0.2
        let gameM = new cc.Node('gameMgr').addComponent(GameMgr)
        cc.game.addPersistRootNode(gameM.node)
        this.LoadADS()
        GameMgr.GameInit()
        GameMgr.LongTimeCountDown = gameM.node.addComponent(LongTimeCountDown)
        this.LoadingAni(bar)
        this.LoadingAB_Res()
        
    }
    /**
    * 加载动画
    */
    private LoadingAni(bar) {
        GameMgr.CycleEventPush({
            cycName: 'Loading', cycFunc: (dt: number) => {
                if (bar.progress < 1) {
                    if (bar.progress < 0.6)
                        this.loadSpeed = 0.2
                    else if (bar.progress < 0.7)
                        this.loadSpeed = 0.1
                    else if (bar.progress < 0.9)
                        this.loadSpeed = 0.05
                    bar.progress += this.loadSpeed * dt
                }
                if (this.judgeFinishLoad) {
                    GameMgr.CycleEventRemove('Loading')
                    this.ani_stop = true
                    this.judgeFinishLoad = false
                    cc.director.loadScene('game', () => {
                        GameMgr.OffLineTime()
                        GameMgr.LongTimeCountDown.PowerAddCountDown()
                        UIManager.ShowPanel(Scene_Data.GamePanel.name, () => { })
                        // ComonTools.PlayBGM(ABName.CommonUse, SourceName.BGM, true)
                    })
                }
            }
        })
    }
    /**
     * 加载广告
     */
    private LoadADS() {
    }
    /**
     * 读取本地数据并开始同步加载游戏资源
     */
    private async LoadingAB_Res(): Promise<void> {
        let data = GameData.LoadDataByLocalStorage()
        console.log("localStorage", data)
        if (data != null && data != '') { GameData.SetData(data) }
        this.ReLoadABPack_Res()
        let t = new Date().getTime()
        await this.AwaitDownLoadAB()
        let t1 = new Date().getTime()
        console.log('AB包加载时间:', t1 - t, 'ms');
        await this.AwaitLoadRes()
        let t2 = new Date().getTime()
        console.log('加载游戏资源时间:', t2 - t1, 'ms');
        cc.director.preloadScene('game', null, async (err) => {
            if (err != null) {
                console.error(err)
                return
            }
            await UIManager.OpenPanel(Scene_Data.GamePanel, false)
            this.judgeFinishLoad = true
        })
    }
    /**
     * 异步加载AB包
     */
    private ReLoadABPack_Res() {

    }
    /**
     * 同步加载AB包
     */
    private async AwaitDownLoadAB(): Promise<void> {

    }
    /**
     * 同步加载资源
     */
    private async AwaitLoadRes(): Promise<void> {
    }
}
