import EventManager from "../manager/EventManager";
import GameData from '../data/GameData';
import GameMgr from '../manager/GameMgr';
import ComonTools from '../tools/ComonTools';
import { ABName, ADSName, SourceName } from '../data/CommonData';
import { LaunchParams, umaStageRunningparams } from "../data/InterfaceData";

const { ccclass } = cc._decorator;

@ccclass
export default class WXSDK {
    private static UserInfoButton: any
    private static canHandleButton: boolean = true
    private static rewardADS: any
    private static reward_index = 0

    /**获取玩家信息*/
    public static GetUserInfo() {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.getUserInfo({
            lang: 'zh_CN',
            success: (res: any) => {
                console.log('getUserInfo', res)
                WXSDK.canHandleButton = false
                let userInfo = res.userInfo
                // GameData.SetCacheUserInfo(userInfo.nickName, userInfo.avatarUrl)
                // LogIn.SetUserInfoToServer(userInfo.nickName, userInfo.avatarUrl)
                EventManager.EventPlay('SetPlayerData')
            },
            fail: (res) => {
                WXSDK.canHandleButton = true
                console.log(res)
                this.CreateButton()
            }
        })
    }
    /**
     * 创建获取用户信息按钮
     */
    public static CreateButton() {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        if (WXSDK.UserInfoButton != null) return
        WXSDK.UserInfoButton = wx.createUserInfoButton({
            type: 'image',
            text: '',
            style: {
                left: 6 / 750 * window.screen.width,
                top: 1.9 + (window.screen.height - 667 - 1) / 2,
                width: 120 / 750 * window.screen.width,
                height: 126 / 1334 * window.screen.height,
                fontSize: 14,
                color: '#9EE32D',
                textAlign: 'center',
                lineHeight: window.screen.height * 0.128,
                borderRadius: 4
            },
            withCredentials: false,
            lang: 'zh_CN'
        })
        WXSDK.UserInfoButton.show()
        WXSDK.UserInfoButton.onTap((res) => {
            if (res.userInfo == null) return
            WXSDK.UserInfoButton.hide()
            WXSDK.GetUserInfo()
        })
    }
    /**
     * 获取用户信息按钮显示
     */
    public static ButtonShow() {
        if (!WXSDK.canHandleButton) return
        if (WXSDK.UserInfoButton == null) return
        WXSDK.UserInfoButton.show()
    }
    /**
     * 获取用户信息按钮隐藏
     */
    public static ButtonHide() {
        if (!WXSDK.canHandleButton) return
        if (WXSDK.UserInfoButton == null) return
        WXSDK.UserInfoButton.hide()
    }
    /**
     * 获取用户openId,uToken
     */
    public static UserLogin(callBack) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.login({
            success(res) {
                if (res.code) {
                    console.log('client login', res)
                    callBack(res.code)
                } else {
                    console.log('登录失败！', res.errMsg)
                }
            }
        })
    }
    /**
     * 发起网络请求
     */
    public static Request(method: string, url: string, data: any, callback: Function, fail?: Function) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.request({
            method: method,
            url: 'https://www.yrhh.net/api/' + url,
            data: data,
            header: { 'content-type': 'application/x-www-form-urlencoded' },
            success: (res) => {
                console.log(url, res)
                callback(res.data)
            },
            fail: (err) => {
                console.error(url, err)
                if (fail != null)
                    fail()
            }
        })
    }
    /**
     * 开启分享功能
     */
    public static ShareAPPMenu() {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.showShareMenu({
            withShareTicket: true,
            menus: ['shareAppMessage', 'shareTimeline']
        })
    }
    /**
     * 修改普通分享的标题与图片
     */
    public static onShareAppMessage() {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.onShareAppMessage(() => {
            return {
                title: '求助！蓝方守不住了！！！',
                imageUrl: 'https://mmocgame.qpic.cn/wechatgame/tkibpDUz6vMSh50fcicopHLO52FK6GLQJQQTXWvoAia4W51rLS7wla3OTU9a0yQLsQic/0' // 图片 URL
            }
        })
        wx.updateShareMenu({
            withShareTicket: true,
            success: () => {
                console.log('withShareTicket success')
            },
            fail: () => {
                console.log('withShareTicket fail')
            }
        })
    }
    /**
     * 通用提示框
     */
    public static showModal(content: string) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.showModal({
            title: '提示',
            content: content,
            showCancel: false
        })
    }
    /**
     * 主动拉起分享窗口
     * @param title 分享标题
     * @param url 图片地址
     * @param id 图片id
     */
    public static ShareApp(title: string, url: string, id: string) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.shareAppMessage({
            title: title,
            imageUrl: url,
            imageUrlId: id
        })
    }
    /**
     * 获取分享信息
     * @param shareTicket 分享卡片信息
     * @param success 成功回调
     */
    public static GetShareInfo(shareTicket: string, success: Function) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.getShareInfo({
            shareTicket: shareTicket,
            success: (res) => {
                console.log('shareInfo', res)
                success(res)
            }
        })
    }
    /**
     * 微信埋点
     * @param branchId 埋点场景ID
     * @param eventType 埋点方式（1：场景曝光；2：事件点击）
     * @param branchDim 埋点额外数据：仅支持1~100正整数
     */
    public static ReportUserBehavior(branchId: string, eventType: number, branchDim = 101) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        if (branchDim > 100) {
            wx.reportUserBehaviorBranchAnalytics({
                branchId: branchId,
                eventType: eventType,
            })
        } else {
            wx.reportUserBehaviorBranchAnalytics({
                branchId: branchId,
                branchDim: branchDim + '',
                eventType: eventType,
            })
        }
    }
    /**
     * 阿拉丁数据发送
     * @param eventName 事件名字
     * @param otherData 其他参数
     */
    public static ALDSendEvent(eventName: string, otherData?: Object) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        if (otherData == null)
            wx.aldSendEvent(eventName);
        else {
            wx.aldSendEvent(eventName, otherData)
        }
    }
    /**
     * 阿拉丁关卡数据--开始
     * @param stageId 关卡ID
     * @param stageName 关卡名称
     */
    public static ALDStart(stageId: string, stageName: string) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.aldStage.onStart({
            stageId: stageId,
            stageName: stageName
        })
    }
    /**
     * 阿拉丁关卡数据--运行
     * @param stageId 关卡ID
     * @param stageName 关卡名称
     * @param event 关卡中用户操作
     * @param params 使用道具名称 该字段必传
     */
    public static ALDRunning(stageId: string, stageName: string, event: string, params: Object) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.aldStage.onRunning({
            stageId: stageId,
            stageName: stageName,
            event: event,
            params: params
        })
    }
    /**
     * 阿拉丁关卡数据--停止
     * @param stageId 关卡ID
     * @param stageName 关卡名称
     * @param event 用户操作
     * @param params 关卡描述
     */
    public static ALDEnd(stageId: string, stageName: string, event: string, params: Object) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.aldStage.onEnd({
            stageId: stageId,
            stageName: stageName,
            event: event,
            params: params
        })
    }
    /**
     * 友盟自定义数据发送
     * @param eventID 事件名字
     * @param otherData 其他参数
     */
    public static UMASendEvent(eventID: string, otherData?: Object) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        otherData == null ? wx.uma.trackEvent(eventID) : wx.uma.trackEvent(eventID, otherData)
    }
    /**
     * 友盟关卡数据--开始
     * @param stageId 关卡ID
     * @param stageName 关卡名称
     */
    public static UMAStart(stageId: string, stageName: string) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.uma.stage.onStart({
            stageId: stageId,
            stageName: stageName
        })
    }
    /**
     * 友盟关卡数据--运行
     * @param stageId 关卡ID
     * @param stageName 关卡名称
     * @param event 关卡中用户操作 (请按照以下两个字段上传，tools/ award 使用道具/关卡中获得奖励)
     * @param params 使用道具名称 该字段必传
     */
    public static UMARunning(stageId: string, stageName: string, event: string, params: umaStageRunningparams) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.uma.stage.onRunning({
            stageId: stageId,
            stageName: stageName,
            event: event,
            params: params
        })
    }
    /**
     * 友盟关卡数据--停止
     * @param stageId 关卡ID
     * @param stageName 关卡名称
     * @param event 关卡结束结果（请按照以下两个字段上传，complete/ fail （关卡完成/关卡失败））
     * @param time 关卡耗时（毫秒）
     */
    public static UMAEnd(stageId: string, stageName: string, event: string, time?: number) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.uma.stage.onEnd({
            stageId,
            stageName,
            event,
            _um_sdu: time,
        })
    }
    public static OnShow(callback: Function) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.onShow((res) => {
            if (callback != null) callback(res)
        })
    }
    public static OnHide(callback: Function) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.onHide((res) => {
            if (callback != null) callback(res)
        })
    }
    /**获取小游戏冷启动时的参数 */
    public static getLaunchOptionsSync(): LaunchParams {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        return wx.getLaunchOptionsSync()
    }
    /**
     * 激励视频预加载
     * @param id 视频广告ID
     * @param videoWatchFinish 视频广告观看完毕
     * @param videoShowFail 视频广告调取失败
     * @param videoWatchFail 视频广告未观看完毕
     */
    public static RewardVideoADS(id: string): any {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        let videoAd = wx.createRewardedVideoAd({
            adUnitId: id
        })
        videoAd.load()
        let load = function () {
            console.log('视频预加载成功')
            videoAd.offLoad()
        }
        videoAd.onLoad(load)
        videoAd.onError(() => {
            console.log('视频加载失败', id)
        })
        this.rewardADS = videoAd
    }
    /**
     * 激励视频播放
     * @param videoAd 视频广告ID
     * @param videoWatchFinish 视频广告观看完毕
     * @param videoShowFail 视频广告调取失败
     * @param videoWatchFail 视频广告未观看完毕
     */
    public static RewardShow(videoWatchFinish: Function, videoShowFail?: Function, videoWatchFail?: Function) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        const closeFunc = (res) => {
            this.rewardADS.offClose(closeFunc)
            if (res && res.isEnded || res === undefined) {
                // GameMgr._watchADSLevel = GameData.GetSingleData('gameLevel') as number
                console.log('视频观看完毕')
                videoWatchFinish()
            } else {
                console.log('rewardADS watch fail')
                console.error('视频未观看完毕')
                if (videoWatchFail != null) videoWatchFail()
            }
        }
        this.rewardADS.onClose(closeFunc)
        this.rewardADS.show().catch(() => {
            // 失败重试
            this.rewardADS.load()
                .then(() => this.rewardADS.show())
                .catch(err => {
                    if (this.reward_index == 0) {
                        this.reward_index = 1
                        // this.RewardVideoADS(WXADSID.backStep_video)
                    } else {
                        this.reward_index = 0
                        // this.RewardVideoADS(WXADSID.addGlass_video)
                    }
                    this.rewardADS.offClose(closeFunc)
                    console.log('激励视频 广告显示失败', err)
                    if (videoShowFail != null) videoShowFail()
                })
        })
    }
    /**
     * 激励视频播放
     * @param id 视频广告ID
     * @param videoWatchFinish 视频广告观看完毕
     * @param videoShowFail 视频广告调取失败
     * @param videoWatchFail 视频广告未观看完毕
     */
    public static RewardADSShow(id: string, videoWatchFinish: Function, videoShowFail?: Function, videoWatchFail?: Function) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        ComonTools.StopBGm()
        let videoAd = wx.createRewardedVideoAd({ adUnitId: id })
        videoAd.onClose((res) => {
            videoAd.offClose()
            // console.log('关闭视频',res, res.isEnd, 'res === undefined', res === undefined);
            if (res && res.isEnded || res === undefined) {
                videoWatchFinish()
                ComonTools.PlayBGM(ABName.CommonUse, SourceName.BGM, true)
            } else {
                console.error('GetPower ad is not finish')
                if (videoWatchFail != null) videoWatchFail()
                ComonTools.PlayBGM(ABName.CommonUse, SourceName.BGM, true)
            }
        })
        videoAd.show().catch(() => {
            // videoAd.offClose()
            // 失败重试
            videoAd.load()
                .then(() => videoAd.show())
                .catch(err => {
                    console.log('激励视频 广告显示失败', err)
                    ComonTools.PlayBGM(ABName.CommonUse, SourceName.BGM, true)
                    if (videoShowFail != null) videoShowFail()
                })
        })
    }
    /**
     * 创建原生广告
     * @param adUnitID 原生广告ID
     * @param adIntervals 更新时长
     * @param left 相对屏幕左框距离
     * @param top 相对于屏幕顶部距离
     * @param fixed 
     */
    public static CreateCustomAD(adUnitID: string, adIntervals: number, left: number, top: number, success: Function, fail: Function): any {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        let customAD = wx.createCustomAd({
            adUnitId: adUnitID,
            adIntervals: adIntervals,
            style: {
                left: left,
                top: top,
            }
        })
        const succ = () => {
            success()
            customAD.offLoad(succ)
            customAD.offError(Err_func)
        }
        customAD.onLoad(succ)
        const Err_func = (res) => {
            console.error('custom', res.errMsg)
            GameMgr.GridADSNone = true
            console.log('GameMgr.GridADSNone', GameMgr.GridADSNone)
            fail()
            customAD.offError(Err_func)
        }
        customAD.onError(Err_func)
        return customAD
    }
    /**
     * 隐藏原生广告
     * @param customAD 原生广告组件
     */
    public static CustomADHide(customAD: any) {
        customAD.hide()
    }
    /**
     * 展示原生广告
     * @param customAD 原生广告组件
     */
    public static CustomADShow(customAD: any) {
        customAD.show()
    }
    /**
     * 销毁原生广告
     * @param customAD 原生广告组件
     */
    public static CustomADDestroy(customAD: any) {
        customAD.destroy()
    }
    /**
     * 创建banner广告
     * @param adUnitId 广告ID
     * @param left 距离屏幕左框位置
     * @param top 距离屏幕顶部位置
     * @param width banner缩放宽度（最小300）
     * @param realLeft 经缩放后真实x坐标
     * @param realTop 经缩放后真实y坐标
     * @param onError 广告创建失败回调
     */
    public static CreateBanner(adUnitId: string, width: number, adIntervals: number, onError?: Function, success?: Function,): any {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        let banner = wx.createBannerAd({
            adUnitId: adUnitId,
            adIntervals: adIntervals,
            style: {
                left: width == null ? 0 : (window.screen.width - width) / 2,
                top: window.screen.height - 300 / (window.screen.width / window.screen.height),
                width: width == null ? window.screen.width : width,
            }
        })
        banner.onResize((res) => {
            banner.style.left = width == null ? 0.1 : (window.screen.width - banner.style.realWidth) / 2;
            banner.style.top = window.screen.height - banner.style.realHeight + 0.1;
        })
        const bannerOnLoad = () => {
            if (success != null) success()
            banner.offLoad(bannerOnLoad)
            banner.offError(bannerErr)
        }
        banner.onLoad(bannerOnLoad)
        let bannerErr = (res) => {
            console.error(adUnitId + '__banner', res.errMsg)
            if (onError != null) onError(res)
            banner.offLoad(bannerOnLoad)
            banner.offError(bannerErr)
        }
        banner.onError(bannerErr)
        return banner
    }
    /**
     * 隐藏banner广告
     * @param banner banner组件
     */
    public static BannerHide(banner: any) {
        banner.hide()
    }
    /**
     * 展示banner广告
     * @param banner banner组件
     */
    public static BannerShow(banner: any) {
        banner.show()
    }
    /**
     * 销毁banner广告
     * @param banner banner组件
     */
    public static BannerDestroy(banner: any) {
        banner.destroy()
    }
    /**
     * 展示或写入子域数据
     * @param type 类型
     * @param data 数据
     * @param event 事件名字
     * @param timer 
     */
    public static OpenDataContext(type: string, data: any, event: string, timer: any) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        let openDataContext = wx.getOpenDataContext();
        openDataContext.postMessage({
            type: type,
            data: data,
            event: event,
            timer: timer
        })
        return openDataContext
    }
    /**
     * 获取右上角功能键位置
     * @returns {width:number,height:number,top:number,right:number,bottom:number,left:number}
     */
    public static GetMBBCRect(): any {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        return wx.getMenuButtonBoundingClientRect()
    }
    /**
     * 短振动
     * @param type 振动类型（heavy,medium,light）
     */
    public static VibrateShort(type: string) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.vibrateShort(type)
    }
    /**
     * 长振动
     */
    public static VibrateLong() {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.vibrateLong()
    }
    /**
     * 创建插屏广告
     * @param id 广告ID
     */
    public static CreateInterstitialAd(id: string, fail?: Function) {
        if (cc.sys.platform != cc.sys.WECHAT_GAME) return
        // 创建插屏广告实例，提前初始化
        let interstitial = wx.createInterstitialAd({ adUnitId: id })
        interstitial.load()
        interstitial.onLoad(() => {
            interstitial.offLoad()
            interstitial.show()
        })
        interstitial.show().catch((err) => {
            console.error('插屏广告显示错误', err)
            if (fail) fail()
        })
        interstitial.onClose(() => {
            interstitial.offClose()
            interstitial.destroy()
        })
    }
    /**
     * 显示消息提示框
     * @param title 提示的内容
     * @param icon 图标
     * @param duration 提示的延迟时间
     */
    public static showToast(title: string, icon: string, duration: number) {
        if(cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.showToast({
            title: title,
            icon: icon,
            duration: duration
        })

    }
    /**
     * 跳转其他小游戏
     */
    public static NavigateGame(appid: string, query: string, succ?: Function, fail?: Function, extraData?: any) {
        if(cc.sys.platform != cc.sys.WECHAT_GAME) return
        wx.navigateToMiniProgram({
            appId: appid,
            path: query,
            extraData: extraData,
            success(res) {
                if (succ != null) succ()
                console.log('navigate success', res)
            },
            fail: (res) => {
                if (fail != null) fail()
                console.error('navigate fail', res)
            }
        })
    }
}
/**
 * 微信广告ID
 */
export const WXADSID = {
    /**常驻原生 */
    custom_ads: '',
    /**全屏原生 */
    big_custom: '',
    /**大屏底部原生 */
    bottom_custom: '',
    /**插屏 */
    interstitialAd: '',
    /**通用视频 */
    common_video: '',
    /**主界面banner */
    main_banner: '',
    /**结算界面banner */
    finish_banner: '',
    /**关卡内banner */
    custom_banner: '',
}
export const WXReportID = {}
