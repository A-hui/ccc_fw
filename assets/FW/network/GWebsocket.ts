/**发送请求的信息 */
type ReqInfo = {
    /**协议编号 */
    cmd: number;
    /**发送的数据 */
    msg: any;
    /**计时器的编号 */
    timerId: number;
    /**已经请求的次数 */
    reqTimes: number;
};

/**
* websocket 通讯相关  
* 需要对应的pb文件
  依赖模块：GEvent
*/
export default class GWebsocket {
    /**命令码映射到对应的函数 */
    private static _CmdMap: Map<number, string> = new Map();

    private static _AllState = {
        /**连接中*/
        Connecting: 'Connecting',
        /**已连接*/
        Connected: 'Connected',
        /**已关闭或未连接*/
        Closed: 'Closed',
    };

    private static _url: string = 'wss://xxx';
    /**本地连接 */
    private static _ws: WebSocket = null;
    /**连接状态 */
    private static _state = 'Closed';
    /**是否开始断线重连 */
    private static _isOffline = false;
    /**重连时间间隔 */
    private static _reconnectTime = 5;
    /**是否能断线重连 */
    private static _canReconnect = true;
    /**请求返回的时间限制，到时间还不返回就重发 */
    private static _resTime = 6;
    /**重发的次数限制 */
    private static _retryTimes = 3;
    /**请求的协议数据 */
    private static _allReqInfo: Map<number, ReqInfo> = new Map();

    /**初始化连接websocket */
    public static init() {
        //初始化命令码映射
        this._createCmdMap();
        //开始连接ws
        this._ws = new WebSocket(this._url);
        this._ws.binaryType = 'arraybuffer';
        this._state = this._AllState.Connecting;
        console.warn('[GWebsocket] init', this._url);
        //添加事件
        this._addEvent();
    }

    /**初始化命令码映射 */
    private static _createCmdMap() {
        if (this._CmdMap.size == 0) {
            //获取pb所有函数
            let all: Map<string, string> = new Map();
            // for (let key of Object.keys(pb)) {
            //     all.set(key.toUpperCase(), key);
            // }
            // for (let key of Object.keys(pb.CMD)) {
            //     let value: number = pb.CMD[key];
            //     let s = key.split("_").join("").toUpperCase();
            //     if (all.get(s)) {
            //         this._CmdMap.set(value, all.get(s));
            //     } else {
            //         console.error("cmd没有对应的处理函数！", key);
            //     }
            // }
        }
    }

    /**添加事件 */
    public static _addEvent() {
        if (!this._ws) {
            cc.error('[GWebsocket] 没有有效的ws');
            return;
        }
        //监听连接
        this._ws.onopen = (event) => {
            console.warn('[GWebsocket] onopen 连接成功！', event);
            this._state = this._AllState.Connected;
            this._canReconnect = true;
            this._isOffline = false;
        };
        //监听关闭
        this._ws.onclose = (event) => {
            console.warn('[GWebsocket] onclose 关闭！', event);
            this._state = this._AllState.Closed;
            //发送事件

            //重连
            this.reconnect();
        };
        //监听出错
        this._ws.onerror = (event) => {
            console.error('[GWebsocket] onerror 出错!', event);
            this._state = this._AllState.Closed;
            //发送事件
            //重连
            this.reconnect();
        };
        //监听接收到信息
        this._ws.onmessage = (event) => {
            console.warn('[GWebsocket] onmessage 接收到信息!', event.data);
            //数据解析
            let uint8Array = new Uint8Array(event.data);
            let bufferArr = Array.from(uint8Array);
            //前面2个字节是cmd
            // let Cmd: pb.CMD = bufferArr.shift();
            // bufferArr.shift();
            // //解析
            // let Info = pb[this._CmdMap.get(Cmd)].decode(bufferArr);
            // console.error("cmd", Cmd, this._CmdMap.get(Cmd), Info);
            // //如果接受到的消息是前一个的回复，就移除计时器
            // let info = this._allReqInfo.get(Cmd - 1);
            // if (info) {
            //     //console.error("移除计时器", Cmd - 1, info.timerId);
            //     console.error("请求次数", info.reqTimes, Cmd - 1, info.timerId);
            //     clearTimeout(info.timerId);
            //     this._allReqInfo.delete(Cmd - 1);
            //     //业务逻辑，消息处理
            //     this._messageHandler(Cmd, Info);
            // } else {
            //     if (this._CmdMap.get(Cmd).indexOf("Rsp") > -1) {
            //         console.error("额外的Rsp不处理");
            //     } else {
            //         this._messageHandler(Cmd, Info);
            //     }
            // }
        };
    }

    /**重新连接 */
    public static reconnect() {
        if (this._canReconnect) {
            this._canReconnect = false;
            console.warn('GWebsocket reconnect 断线重连');
            this._isOffline = true;
            this.close();
            this.init();
        } else {
            setTimeout(() => {
                this._canReconnect = true;
                if (this._state != this._AllState.Connected) {
                    this.reconnect();
                }
            }, this._reconnectTime * 1000);
        }
    }

    /**关闭连接 */
    public static close() {
        if (this._ws) {
            this._state = this._AllState.Closed;
            this._ws.onopen = null;
            this._ws.onclose = null;
            this._ws.onerror = null;
            this._ws.onmessage = null;
            this._ws.close();
            this._ws = null;
        }
    }

    /**向服务端发送信息 */
    // send(cmd: pb.CMD, msg?: any, canRecord = true, isRetry = false) {
    //     if (this._allReqInfo.has(cmd) && !isRetry) {
    //         console.error("已经请求的协议", cmd, this._allReqInfo.get(cmd));
    //     } else {
    //         console.warn("[GWebsocket] send 向服务端发送信息", cmd, msg);
    //         let info = this._allReqInfo.get(cmd);
    //         if (info) {
    //             info.reqTimes++;
    //         } else {
    //             //记录
    //             info = {
    //                 /**协议编号 */
    //                 cmd,
    //                 /**发送的数据 */
    //                 msg,
    //                 /**计时器的编号 */
    //                 timerId: 0,
    //                 /**已经请求的次数 */
    //                 reqTimes: 1,
    //             };
    //         }
    //         this._allReqInfo.set(cmd, info);
    //         //开始发送倒计时，时间到了就再次发送
    //         let id = <any>setTimeout(() => {
    //             console.error("重新开始发送", id, cmd, info);
    //             this.send(cmd, msg, canRecord, true);
    //         }, this._resTime * 1000);
    //         info.timerId = id;
    //         if (this._state != this._AllState.Connected) {
    //             console.warn("[GWebsocket] send ws为连接!", cmd, msg);
    //             //开始重连
    //             this.reconnect();
    //         } else {
    //             let buffer: Uint8Array = pb[this._CmdMap.get(cmd)].encode(msg).finish();
    //             let bufferArr = Array.from(buffer);
    //             let buffInfo = new Uint8Array([cmd, 0, ...bufferArr]);
    //             if (info.reqTimes > this._retryTimes) {
    //                 console.error("重试超过次数!!!", info);
    //             } else {
    //                 this._ws.send(buffInfo.buffer);
    //             }
    //         }
    //     }
    // }

    /**对message进行处理，业务逻辑 */
    // private _messageHandler(Cmd: pb.CMD, Info: any) {
    //     //数据逻辑
    //     switch (Cmd) {
    //         case pb.CMD.PLAYER_INFO_NTY://断线重连后会再次请求登录
    //             break;
    //         default:
    //             break;
    //     }
    //     //发送事件，表现逻辑?
    //     GEvent.emit(Cmd, Info);
    // }
}