import GameConfig, { HealthAdvice } from "../FW/config/GameConfig";
import CacheData from "../FW/data/CacheData";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Loading extends cc.Component {
    @property(cc.ProgressBar)
    public bar: cc.ProgressBar = null
    onLoad(): void {
        let s = setTimeout(() => {
            clearTimeout(s)
            // this.LabelAni()
            this.showHealthAdvice()
            this.StartLoading()
        }, 0);
    }
    /**
     * 文字动画
     * @param index 下标
     */
    private LabelAni(index = 0) {
        // if (this.ani_stop) return
        // if (index >= 3) {
        //     setTimeout(() => {
        //         if (this.point_arr == null || this.point_arr.length <= 0) return
        //         for (let i = 0; i < this.point_arr.length; i++)
        //             this.point_arr[i].enabled = false
        //         setTimeout(() => {
        //             this.LabelAni()
        //         }, 300);
        //     }, 1000);
        //     return
        // }
        // this.point_arr[index].enabled = true
        // index++
        // setTimeout(() => {
        //     this.LabelAni(index)
        // }, 500);
    }
    /**
     * 开始进度条动画
     */
    private StartLoading() {
        this.LoadFW(() => {
            console.log('loading0')
            this.node.addComponent("GameInit")._GameInit(this.bar)
        })
    }

    /**显示健康忠告 */
    private showHealthAdvice() {
        if (HealthAdvice.isShow && CacheData.isFirstLoading) {
            const fadeOutTimeS = 0.5 // 健康忠告渐隐动画的时长
            const panel = this.node.getChildByName('bg').getChildByName('tip')
            panel.active = true

            if (HealthAdvice.showTimeS - fadeOutTimeS < 0) {
                throw new Error('渐隐时间不能大于健康忠告播放时间')
            }

            this.scheduleOnce(() => {
                const outAction = cc.fadeOut(fadeOutTimeS)
                panel.runAction(outAction)
                this.scheduleOnce(() => {
                    panel.active = false
                }, fadeOutTimeS)
            }, HealthAdvice.showTimeS - fadeOutTimeS)
        }
    }

    /**
     * 加载框架与代码
     */
    private LoadFW(callback: Function) {
        cc.assetManager.loadBundle('FW', null, (err, fw) => {
            if (err != null) {
                console.log(err)
                return null
            }
            callback()
            cc.assetManager.loadBundle('Script', null, (err, script) => {
                if (err != null) {
                    console.log(err)
                    return null
                }
            })
        })
    }
}